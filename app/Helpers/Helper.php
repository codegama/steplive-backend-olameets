<?php 

namespace App\Helpers;

use Mailgun\Mailgun;

use Validator, Hash, Exception, Auth, Mail, File, Log, Storage, Setting, DB;

use App\Admin, App\User, App\Settings, App\StaticPage;

class Helper {

    public static function clean($string) {

        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public static function generate_token() {
        
        return Helper::clean(Hash::make(rand() . time() . rand()));
    }

    public static function generate_token_expiry() {

        $token_expiry_hour = Setting::get('token_expiry_hour') ? Setting::get('token_expiry_hour') : 1;
        
        return time() + $token_expiry_hour*3600;  // 1 Hour
    }

    // Note: $error is passed by reference
    
    public static function is_token_valid($entity, $id, $token, &$error) {

        if (
            ( $entity== USER && ($row = User::where('id', '=', $id)->where('token', '=', $token)->first()))
        ) {

            if ($row->token_expiry > time()) {
                // Token is valid
                $error = NULL;
                return true;
            } else {
                $error = ['success' => false, 'error' => api_error(1003), 'error_code' => 1003];
                return FALSE;
            }
        }

        $error = ['success' => false, 'error' => api_error(1004), 'error_code' => 1004];
        return FALSE;
   
    }

    public static function generate_email_code($value = "") {

        return uniqid($value);
    }

    public static function generate_email_expiry() {

        $token_expiry = Setting::get('token_expiry_hour') ?: 1;
            
        return time() + $token_expiry*3600;  // 1 Hour

    }

    // Check whether email verification code and expiry

    public static function check_email_verification($verification_code , $user_id , &$error) {

        if(!$user_id) {

            $error = tr('user_id_empty');

            return FALSE;

        } else {

            $user_details = User::find($user_id);
        }

        // Check the data exists

        if($user_details) {

            // Check whether verification code is empty or not

            if($verification_code) {

                // Log::info("Verification Code".$verification_code);

                // Log::info("Verification Code".$user_details->verification_code);

                if ($verification_code ===  $user_details->verification_code ) {

                    // Token is valid

                    $error = NULL;

                    // Log::info("Verification CODE MATCHED");

                    return true;

                } else {

                    $error = tr('verification_code_mismatched');

                    // Log::info(print_r($error,true));

                    return FALSE;
                }

            }
                
            // Check whether verification code expiry 

            if ($user_details->verification_code_expiry > time()) {

                // Token is valid

                $error = NULL;

                Log::info(tr('token_expiry'));

                return true;

            } else if($user_details->verification_code_expiry < time() || (!$user_details->verification_code || !$user_details->verification_code_expiry) ) {

                $user_details->verification_code = Helper::generate_email_code();
                
                $user_details->verification_code_expiry = Helper::generate_email_expiry();
                
                $user_details->save();

                // If code expired means send mail to that user

                $subject = tr('verification_code_title');
                $email_data = $user_details;
                $page = "emails.welcome";
                $email = $user_details->email;
                $result = Helper::send_email($page,$subject,$email,$email_data);

                $error = tr('verification_code_expired');

                Log::info(print_r($error,true));

                return FALSE;
            }
       
        }

    }
    
    public static function generate_password() {

        $new_password = time();

        $new_password .= rand();

        $new_password = sha1($new_password);

        $new_password = substr($new_password, 0,8);

        return $new_password;
    
    }

    public static function file_name() {

        $file_name = time();

        $file_name .= rand();

        $file_name = sha1($file_name);

        return $file_name;    
    }

    public static function web_url() 
    {
        return url('/');
    }
    

     public static function upload_file($picture , $folder_path = COMMON_FILE_PATH) {
       
        $file_path_url = "";

        $file_name = Helper::file_name();

        $ext = $picture->getClientOriginalExtension();

        $local_url = $file_name . "." . $ext;

        $inputFile = base_path('public'.$folder_path.$local_url);

        $picture->move(public_path().$folder_path, $local_url);

        $file_path_url = Helper::web_url().$folder_path.$local_url;

        return $file_path_url;
    
    }


    public static function delete_file($picture, $path = COMMON_FILE_PATH) {

        if(file_exists(public_path().$path.basename($picture))) {

            File::delete(public_path().$path.basename($picture));
      
        } else {

            return false;
        }  

        return true;    
    
    }
 
    public static function send_email($page,$subject,$email,$email_data) {

       return true;
    
    }

    public static function custom_validator($request, $request_inputs, $custom_errors = []) {
        
        $validator = Validator::make($request, $request_inputs, $custom_errors);

        if($validator->fails()) {

            $error = implode(',', $validator->messages()->all());
            
            throw new Exception($error, 101);
               
        }
    }

    /**
     * @method settings_generate_json()
     *
     * @uses used to update settings.json file with updated details.
     *
     * @created vidhya
     * 
     * @updated vidhya
     *
     * @param -
     *
     * @return boolean
     */
    
    public static function settings_generate_json() {

        $basic_keys = ['site_name', 'site_logo', 'site_icon', 'tag_name','currency', 'currency_code', 'google_analytics', 'header_scripts', 'body_scripts', 'facebook_link', 'linkedin_link', 'twitter_link', 'google_plus_link', 'pinterest_link', 'demo_user_email', 'demo_user_password', 'appstore', 'playstore', 'meta_title', 'meta_description', 'meta_author', 'meta_keywords'];

        $settings = Settings::get();

        $sample_data = [];

        foreach ($settings as $key => $setting_details) {

            $sample_data[$setting_details->key] = $setting_details->value;
        }
        $static_page_ids1 = ['about', 'terms', 'privacy', 'contact'];

        $footer_pages1 = StaticPage::CommonResponse()->where('section_type', STATIC_PAGE_SECTION_1)->where('status', APPROVED)->get();

        $footer_pages2 = StaticPage::CommonResponse()->where('section_type', STATIC_PAGE_SECTION_2)->where('status', APPROVED)->get();

        $footer_pages3 = StaticPage::CommonResponse()->where('section_type', STATIC_PAGE_SECTION_3)->where('status', APPROVED)->get();

        $sample_data['footer_pages1'] = $footer_pages1;

        $sample_data['footer_pages2'] = $footer_pages2;

        $sample_data['footer_pages3'] = $footer_pages3;

        $data['data'] = $sample_data;

        $data = json_encode($data);

        $file_name = public_path('default-json/settings.json');

        File::put($file_name, $data);
   
    }

    /**
     * @method upload_file
     */
    
    public static function storage_upload_file($input_file, $folder_path = COMMON_FILE_PATH) {

        $url = "";

        if($input_file) {
       
            $name = Helper::file_name();

            $ext = $input_file->getClientOriginalExtension();

            $file_name = $name.".".$ext;

            $public_folder_path = "public/".$folder_path;

            Storage::putFileAs($public_folder_path, $input_file, $file_name);

            $storage_file_path = $folder_path.$file_name;

            $url = asset(Storage::url($storage_file_path));
        }
        
        return $url;

    }

    /**
     * @method
     * 
     */
    public static function storage_delete_file($url, $folder_path = COMMON_FILE_PATH) {

        $file_name = basename($url);

        $storage_file_path = $folder_path.$file_name;

        Storage::delete($storage_file_path);
    }

}