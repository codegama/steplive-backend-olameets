<?php

namespace App\Http\Controllers\UserApi;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Repositories\MeetingRepository as MeetingRepo;

use DB, Log, Hash, Validator, Exception, Setting, Helper;

use App\User;

class MeetingApiController extends Controller
{
    protected $loginUser;

    protected $skip, $take;

	public function __construct(Request $request) {

        Log::info(url()->current());

        Log::info("Request Data".print_r($request->all(), true));
        
        $this->loginUser = User::find($request->id);

        $this->skip = $request->skip ?: 0;

        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

        $this->timezone = $this->loginUser->timezone ?? "America/New_York";

    }

    /**
     * @method meetings_index()
     *
     * @uses list rooms created by logged user
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_index(Request $request) {

        try {

            $base_query = $total_query = \App\Meeting::where('user_id', $request->id)->orderBy('created_at', 'desc');

            $meetings = $base_query->skip($this->skip)->take($this->take)->get();

            $meetings = MeetingRepo::meetings_list_response($meetings, $request);

            $data['meetings'] = $meetings;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_upcoming()
     *
     * @uses list rooms created by logged user
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_upcoming(Request $request) {

        try {

            $base_query = $total_query = \App\Meeting::where('user_id', $request->id)->where('status', MEETING_SCHEDULED)->orderBy('created_at', 'desc');

            $meetings = $base_query->skip($this->skip)->take($this->take)->get();

            $meetings = MeetingRepo::meetings_list_response($meetings, $request);

            $data['meetings'] = $meetings;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_view()
     *
     * @uses room view based on the logged in user
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_view(Request $request) {

        try {

            $meeting_details = \App\Meeting::where('meetings.unique_id', $request->meeting_unique_id)->first();

            $meeting_details->start_btn_status = $meeting_details->end_btn_status = $meeting_details->cancel_btn_status =$meeting_details->join_btn_status =  NO;

            if($request->id == $meeting_details->user_id) {

                $meeting_details->start_btn_status = $meeting_details->status == MEETING_SCHEDULED ? YES : NO;

                $meeting_details->end_btn_status = $meeting_details->status == MEETING_STARTED ? YES : NO;

                $meeting_details->cancel_btn_status = in_array($meeting_details->status, [MEETING_SCHEDULED]) ? YES : NO;

            } else {

                $meeting_details->join_btn_status = $meeting_details->status == MEETING_STARTED ? YES : NO;

            }

            $meeting_details->schedule_time = common_date($meeting_details->schedule_time, $this->timezone, 'd M Y h:i A');

            $meeting_details->start_time = common_date($meeting_details->start_time, $this->timezone, 'd M Y h:i A');

            $meeting_details->end_time = common_date($meeting_details->end_time, $this->timezone, 'd M Y h:i A');

            $data['meeting_details'] = $meeting_details;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }
    
    /**
     * @method meetings_now_save()
     *
     * @uses room details save or create
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_now_save(Request $request) {

        try {

            // Check the user is subscribed any plan

            DB::beginTransaction();

            $meeting_details = new \App\Meeting;

            $meeting_details->user_id = $request->id;

            $meeting_details->title = $request->title ?: "My Meeting ".rand(10000, 99999);

            $meeting_details->description = $request->description ?: "My Meeting ".rand(10000, 99999);

            $meeting_details->schedule_time = $meeting_details->start_time = date('Y-m-d H:i:s');

            $subscription_payment = \App\SubscriptionPayment::where('user_id', $request->id)->where('is_current_subscription', YES)->first();

            $no_of_users = Setting::get('max_no_of_users', 3);

            $no_of_minutes = Setting::get('max_no_of_minutes', 30);

            if($subscription_payment) {

                $no_of_users = $subscription_payment->no_of_users ?? 5;

                $no_of_minutes = $subscription_payment->no_of_minutes ?? 5;

            }

            $meeting_details->no_of_users = $no_of_users;

            $meeting_details->no_of_minutes = $no_of_minutes;

            $meeting_details->save();

            $response = \App\Repositories\BBBRepository::bbb_meeting_create($request, $meeting_details)->getData();

            if($response->success == false) {

                throw new Exception($response->error, $response->error_code);
            }

            $meeting_user = new \App\MeetingUser;

            $meeting_user->meeting_id = $meeting_details->meeting_id;

            $meeting_user->user_id = $request->id ?: 0;

            $meeting_user->username = $this->loginUser->name ?? "User".rand(10000, 99999);

            $meeting_user->start_time = date('Y-m-d H:i:s');

            $meeting_user->save();

            $attendeePW = $moderatorPW = '';

            if($request->id == $meeting_details->user_id) {

                $moderatorPW = 'mp'.$meeting_details->meeting_id;

            } else {

                $attendeePW = 'ap'.$meeting_details->meeting_id;

            }

            $request->request->add(['username' => $meeting_user->username,'moderatorPW' => $moderatorPW, 'attendeePW' => $attendeePW]);

            $response = \App\Repositories\BBBRepository::bbb_meeting_join($request, $meeting_details)->getData();

            if($response->success == false) {

                throw new Exception($response->error, $response->error_code);
            }

            DB::commit();

            $data['meeting_details'] = $meeting_details;

            $data['bbb_response'] = $response;

            $data['join_url'] = $response->data;

            DB::commit();

            return $this->sendResponse($message = api_success(119) , $code = 119, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_save()
     *
     * @uses room details save or create
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_save(Request $request) {

        try {

            // Check the user is subscribed any plan

            DB::beginTransaction();

            // Validation start

            $rules = [
                    'title' => 'required|max:255',
                    'description' => 'required|max:255',
                    'schedule_time' => 'required',
                    'picture' => 'nullable|mimes:jpeg,jpg,png',
                ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting_details = new \App\Meeting;

            $meeting_details->user_id = $request->id;

            $meeting_details->title = $request->title;

            $meeting_details->description = $request->description ?? "";

            $meeting_details->schedule_time = $request->schedule_time ? date('Y-m-d H:i:s', strtotime($request->schedule_time)) : date("Y-m-d H:i:s", strtotime(date("Y-m-d H:i:s")." +5 minutes"));

            $subscription_payment = \App\SubscriptionPayment::where('user_id', $request->id)->where('is_current_subscription', YES)->first();

            $no_of_users = Setting::get('max_no_of_users', 3);

            $no_of_minutes = Setting::get('max_no_of_minutes', 30);

            if($subscription_payment) {

                $no_of_users = $subscription_payment->no_of_users ?? 5;

                $no_of_minutes = $subscription_payment->no_of_minutes ?? 5;

            }

            $meeting_details->no_of_users = $no_of_users;

            $meeting_details->no_of_minutes = $no_of_minutes;

            if($request->file('picture')) {

                $meeting_details->picture = Helper::storage_upload_file($request->file('picture'), MEETING_PATH);

            }

            $meeting_details->save();

            $response = \App\Repositories\BBBRepository::bbb_meeting_create($request, $meeting_details)->getData();

            if($response->success == false) {

                throw new Exception($response->error, $response->error_code);
            }

            DB::commit();

            $data['meeting_details'] = $meeting_details;

            $data['bbb_response'] = $response;

            return $this->sendResponse($message = api_success(119) , $code = 119, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }
    
    /**
     * @method meetings_joing()
     *
     * @uses join the meeting room
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_join(Request $request) {

        try {

            // Check the user is subscribed any plan

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_unique_id' => 'required|exists:meetings,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting_details = \App\Meeting::where('meetings.unique_id', $request->meeting_unique_id)->first();

            if(!$meeting_details) {
                throw new Exception(api_error(139), 139);
            }

            if($meeting_details->status != MEETING_STARTED) {

                // throw new Exception(api_error(141), 141);

            }

            $meeting_user = new \App\MeetingUser;

            $meeting_user->meeting_id = $meeting_details->meeting_id;

            $meeting_user->user_id = $request->id ?: 0;

            $meeting_user->username = $request->username ?: ($this->loginUser->name ?? "User".rand(10000, 99999));

            $meeting_user->start_time = date('Y-m-d H:i:s');

            $meeting_user->save();

            $attendeePW = $moderatorPW = '';

            if($request->id == $meeting_details->user_id) {

                $moderatorPW = 'mp'.$meeting_details->meeting_id;

                $meeting_details->start_time = date('Y-m-d H:i:s');

                $meeting_details->save(); 

            } else {

                $attendeePW = 'ap'.$meeting_details->meeting_id;

            }

            $request->request->add(['username' => $meeting_user->username,'moderatorPW' => $moderatorPW, 'attendeePW' => $attendeePW]);

            $response = \App\Repositories\BBBRepository::bbb_meeting_join($request, $meeting_details)->getData();

            if($response->success == false) {

                throw new Exception($response->error, $response->error_code);
            }

            DB::commit();

            $data['meeting_details'] = $meeting_details;

            $data['join_url'] = $response->data;

            return $this->sendResponse($message = api_success(125) , $code = 125, $data);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_end()
     *
     * @uses room details save or create
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_end(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_id' => 'required|exists:meetings,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting_details = \App\Meeting::where('unique_id', $request->meeting_id)->first();

            if(!$meeting_details) {
                throw new Exception(api_error(139), 139);
            }

            $meeting_details->status = MEETING_ENDED;

            $meeting_details->end_time = date('Y-m-d H:i:s');

            $meeting_details->save();

            DB::commit();

            $frontend_url = Setting::get('frontend_url').'meetings';

            return redirect()->away($frontend_url);

            return $this->sendResponse($message = api_success(119) , $code = 119, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            $frontend_url = Setting::get('frontend_url').'meetings';

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_delete()
     *
     * @uses sub instructors remove
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_delete(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_id' => 'required|exists:meetings,id'];

            $custom_errors = ['meeting_id' => api_error(139)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $meeting_details = \App\Meeting::where('meetings.id', $request->meeting_id)->where('user_id', $request->id)->delete();

            DB::commit();

            return $this->sendResponse($message = api_success(120) , $code = 120, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meetings_search()
     *
     * @uses meetings search
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meetings_search(Request $request) {

        try {

            // Validation start

            $rules = ['search_key' => 'required|min:2'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $base_query = $total_query = \App\Meeting::where('title','LIKE','%'.$request->search_key.'%');

            $meetings = $base_query->skip($this->skip)->take($this->take)->get();

            $data['meetings'] = $meetings;

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_users_index()
     *
     * @uses list users under selected meeting
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_users_index(Request $request) {

        try {

            // Validation start

            $rules = ['meeting_id' => 'required|exists:meetings,id'];

            $custom_errors = ['meeting_id' => api_error(132)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $total_query = $base_query = \App\MeetingUser::where('meeting_id', $request->meeting_id);

            $meeting_users = $base_query->skip($this->skip)->take($this->take)->get();

            $data['meeting_users'] = $meeting_users;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_users_search()
     *
     * @uses meeting users search based on the key
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_users_search(Request $request) {

        try {

            // Validation start

            $rules = [
                        'search_key' => 'required|min:2', 
                        'meeting_id' => 'required|exists:meetings,id'
                    ];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting_user_ids = \App\MeetingUser::where('meeting_id', $request->meeting_id)->list('user_id');

            $base_query = $total_query = \App\User::whereNotIn('users.id', '!=', $meeting_user_ids)
                            ->where('name','LIKE','%'.$request->search_key.'%')
                            ->orWhere('email','LIKE','%'.$request->search_key.'%')
                            ->orWhere('mobile','LIKE','%'.$request->search_key.'%');

            $users = $user_query->skip($this->skip)->take($this->take)->get();

            $data['users'] = $users;

            $data['total'] = $total_query->count();

            return $this->sendResponse($message = "", $code = "", $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_records_index()
     *
     * @uses list of the recordings
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_records_index(Request $request) {

        try {

            $base_query = $total_query = \App\MeetingRecord::where('user_id', $request->id)->orderBy('created_at', 'desc');

            $meeting_records = $base_query->skip($this->skip)->take($this->take)->get();

            $data['records'] = $meeting_records;

            $data['total'] = $total_query->count() ?: 0;

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_records_view()
     *
     * @uses view recording details
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_records_view(Request $request) {

        try {

            $record_details = \App\MeetingRecord::where('unique_id', $request->meeting_record_unique_id)->first();

            if(!$record_details) {
                throw new Exception(api_error(140), 140);
            }

            $data['record_details'] = $record_details;

            $data['meeting_details'] = \App\Meeting::where('id', $record_details->meeting_id)->first();

            return $this->sendResponse($message = '' , $code = '', $data);

        } catch(Exception $e) {

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method meeting_records_save()
     *
     * @uses meeting records end
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_records_save(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_id' => 'required|exists:meetings,unique_id'];

            Helper::custom_validator($request->all(), $rules, $custom_errors = []);

            // Validation end

            $meeting_details = \App\Meeting::where('meetings.unique_id', $request->meeting_id)->first();

            if(!$meeting_details) {
                throw new Exception(api_error(139), 139);
            }

            // $response = \App\Repositories\BBBRepository::getRecordings($request, $meeting_details)->getData();

            $response = \App\Repositories\BBBRepository::PublishRecordings($request, $meeting_details)->getData();

            if($response->success == false) {

                throw new Exception($response->error, $response->error_code);
            }

            dd($response);

            // get the meeting records

            $meeting_record = new \App\MeetingRecord;

            $meeting_record->meeting_id = $request->meeting_id ?: rand(1000, 2000);

            $meeting_record->user_id = 1;

            $meeting_record->save();

            DB::commit();

            return $this->sendResponse($message = api_success(120) , $code = 120, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }

    /**
     * @method recordings_delete()
     *
     * @uses sub instructors remove
     *
     * @created vithya R
     *
     * @updated Vidhya R
     *
     * @param request id
     *
     * @return JSON Response
     */
    public function meeting_records_delete(Request $request) {

        try {

            DB::beginTransaction();

            // Validation start

            $rules = ['meeting_record_id' => 'required|exists:meeting_records,id'];

            $custom_errors = ['meeting_record_id' => api_error(140)];

            Helper::custom_validator($request->all(), $rules, $custom_errors);

            // Validation end

            $meeting_details = \App\MeetingRecord::where('meeting_records.id', $request->meeting_record_id)->where('user_id', $request->id)->delete();

            DB::commit();

            return $this->sendResponse($message = api_success(120) , $code = 120, $data = []);

        } catch(Exception $e) {

            DB::rollback();

            return $this->sendError($e->getMessage(), $e->getCode());
        
        }
    
    }
}
