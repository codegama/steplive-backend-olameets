<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Helper, App\Helpers\EnvEditorHelper;

use DB, Hash, Setting, Auth, Validator, Exception, Enveditor;

use App\Admin, App\User;

use App\Settings, App\StaticPage;

use App\Jobs\SendEmailJob;

use Carbon\Carbon;

use App\Subscription, App\SubscriptionPayment, App\Meeting, App\MeetingUser, App\MeetingRecord;

class AdminController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {

        $this->middleware('auth:admin');

        $this->skip = $request->skip ?: 0;
       
        $this->take = $request->take ?: (Setting::get('admin_take_count') ?: TAKE_COUNT);

    }

    /**
     * @method index()
     *
     * @uses Show the application dashboard.
     *
     * @created vithya
     *
     * @updated vithya
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function index() {
        
        $data = new \stdClass;

        $data->total_users = User::count();

        $data->today_users = User::whereDate('created_at',today())->count();

        $data->total_revenue = SubscriptionPayment::where('status', PAID)->sum('amount');
        
        $data->today_revenue = SubscriptionPayment::whereDate('paid_date',today())->sum('amount');

        $today_subscriptions  = SubscriptionPayment::whereDate('paid_date', today())->where('status', PAID)->take(5)->get();

        $data->on_live_meetings = Meeting::where('status',APPROVED)->orderBy('id' , 'desc')->where('meeting_type',MEETING_TYPE_LIVE)->skip(0)->take(6)->get();
        
        $data->today_scheduled_meetings = Meeting::where('status',APPROVED)->orderBy('id' , 'desc')->where('schedule_time','!=',NULL)->skip(0)->take(6)->get();

        $data->total_meetings = Meeting::where('schedule_time','!=', NULL)->count();
        
        $data->scheduled_meetings = Meeting::where('status', MEETING_SCHEDULED)->where('schedule_time','!=', NULL)->count();

        $data->today_meetings = Meeting::whereIn('status', [MEETING_SCHEDULED, MEETING_STARTED])->whereDate('created_at',today())->count();

        $data->completed_meetings = Meeting::where('status', MEETING_ENDED)->count();

        $data->cancelled_meetings = Meeting::where('status', MEETING_CANCELLED)->count();

        $data->analytics = last_x_days_revenue(6);
        
        return view('admin.dashboard')
                    ->with('main_page' , 'dashboard')
                    ->with('data', $data)
                    ->with('today_subscriptions',$today_subscriptions);
    
    }

     /**
     * @method users_index()
     *
     * @uses To list out users details 
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function users_index(Request $request) {

        $base_query = User::orderBy('created_at','desc');

        if($request->today) {

            $base_query = $base_query->whereDate('created_at',today());
        }

        if($request->search_key) {

            $base_query = $base_query
                    ->orWhere('name','LIKE','%'.$request->search_key.'%')
                    ->orWhere('email','LIKE','%'.$request->search_key.'%')
                    ->orWhere('mobile','LIKE','%'.$request->search_key.'%');
        }

        if($request->status) {

            switch ($request->status) {

                case SORT_BY_APPROVED:
                    $base_query = $base_query->where('status',APPROVED);
                    break;

                case SORT_BY_DECLINED:
                    $base_query = $base_query->where('status',DECLINED);
                    break;

                case SORT_BY_EMAIL_VERIFIED:
                    $base_query = $base_query->where('is_verified',USER_EMAIL_VERIFIED);
                    break;
                
                default:
                    $base_query = $base_query->where('is_verified',USER_EMAIL_NOT_VERIFIED);
                    break;
            }
        }

        $users = $base_query->paginate($this->take);

        return view('admin.users.index')
                    ->with('main_page','users-crud')
                    ->with('page','users')
                    ->with('sub_page' , 'users-view')
                    ->with('users' , $users);
    }

    /**
     * @method users_create()
     *
     * @uses To create user details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function users_create() {

        $user_details = new User;

        return view('admin.users.create')
                    ->with('main_page','users-crud')
                    ->with('page' , 'users')
                    ->with('sub_page','users-create')
                    ->with('user_details', $user_details);           
    }

    /**
     * @method users_edit()
     *
     * @uses To display and update user details based on the user id
     *
     * @created Anjana
     *
     * @updated Anjana
     *
     * @param object $request - User Id
     * 
     * @return redirect view page 
     *
     */
    public function users_edit(Request $request) {

        try {

            $user_details = User::find($request->user_id);

            if(!$user_details) { 

                throw new Exception(tr('user_not_found'), 101);
            }

            return view('admin.users.edit')
                    ->with('main_page','users-crud')
                    ->with('page' , 'users')
                    ->with('sub_page','users-view')
                    ->with('user_details' , $user_details); 
            
        } catch(Exception $e) {

            return redirect()->route('admin.users.index')->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method users_save()
     *
     * @uses To save the users details of new/existing user object based on details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object request - User Form Data
     *
     * @return success message
     *
     */
    public function users_save(Request $request) {

        try {

            DB::begintransaction();

            $rules = [
                'name' =>  $request->user_id ? 'required|max:191' : 'required|max:191|unique:users,name',
                'email' => $request->user_id ? 'required|email:rfc,dns|email|max:191|unique:users,email,'.$request->user_id.',id' : 'required|email:rfc,dns|email|max:191|unique:users,email,NULL,id',
                'password' => $request->user_id ? "" : 'required|min:6',
                'mobile' => '',
                'picture' => 'mimes:jpg,png,jpeg',
                'user_id' => 'exists:users,id'
            ];

            Helper::custom_validator($request->all(),$rules);

            $user_details = $request->user_id ? User::find($request->user_id) : new User;

            $is_new_user = NO;

            if($user_details->id) {

                $message = tr('user_updated_success'); 

            } else {

                $is_new_user = YES;

                $user_details->password = ($request->password) ? \Hash::make($request->password) : NULL;

                $message = tr('user_created_success');

                $user_details->email_verified_at = date('Y-m-d H:i:s');

                $user_details->picture = asset('placeholder.jpeg');

                $user_details->is_verified = USER_EMAIL_VERIFIED;

            }

            $user_details->name = $request->name ?: $user_details->name;

            $user_details->email = $request->email ?: $user_details->email;

            $user_details->mobile = $request->mobile ?: '';

            $user_details->login_by = $request->login_by ?: 'manual';

            // Upload picture
            
            if($request->hasFile('picture')) {

                if($request->user_id) {

                    Helper::storage_delete_file($user_details->picture, COMMON_FILE_PATH); 
                    // Delete the old pic
                }

                $user_details->picture = Helper::storage_upload_file($request->file('picture'), COMMON_FILE_PATH);
            }

            if($user_details->save()) {

                if($is_new_user == YES) {

                    /**
                     * @todo Welcome mail notification
                     */

                    $user_details->is_verified = USER_EMAIL_VERIFIED;

                    $user_details->save();

                }

                DB::commit(); 

                return redirect(route('admin.users.view', ['user_id' => $user_details->id]))->with('flash_success', $message);

            } 

            throw new Exception(tr('user_save_failed'));
            
        } catch(Exception $e){ 

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());

        } 

    }

    /**
     * @method users_view()
     *
     * @uses view the users details based on users id
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - User Id
     * 
     * @return View page
     *
     */
    public function users_view(Request $request) {
       
        try {
      
            $user_details = User::find($request->user_id);

            if(!$user_details) { 

                throw new Exception(tr('user_not_found'), 101);                
            }

            $base_query = Meeting::where('user_id',$user_details->id);

            $user_details->total_meetings = Meeting::where('user_id',$user_details->id)->count();

            $user_details->today_meetings = Meeting::where('user_id',$user_details->id)->whereDate('created_at',today())->count();

            $user_id = $user_details->id;


            $user_details->upcoming_meetings = 
                                    Meeting::where('meetings.status',MEETING_TYPE_UPCOMING)
                                    ->whereHas('meetingUsers', function($q) use ($user_id){

                                        $q->where('meeting_users.user_id',$user_id);
                                    })->count();

            $user_details->cancelled_meetings = 
                                    Meeting::where('meetings.status',MEETING_TYPE_CANCELLED)
                                    ->whereHas('meetingUsers', function($q) use ($user_id){

                                        $q->where('meeting_users.user_id',$user_id);
                                    })->count();

            
            $data = new \stdClass;

            $data->analytics = last_x_days_upcoming_bookings(10,$user_details->id);

            return view('admin.users.view')
                        ->with('main_page','users-crud')
                        ->with('page', 'users') 
                        ->with('sub_page','users-view') 
                        ->with('user_details' , $user_details)
                        ->with('data' , $data);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method users_delete()
     *
     * @uses delete the user details based on user id
     *
     * @created Akshata 
     *
     * @updated  
     *
     * @param object $request - User Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function users_delete(Request $request) {

        try {

            DB::begintransaction();

            $user_details = User::find($request->user_id);
            
            if(!$user_details) {

                throw new Exception(tr('user_not_found'), 101);                
            }

            if($user_details->delete()) {

                DB::commit();

                return redirect()->back()->with('flash_success',tr('user_deleted_success'));   

            } 
            
            throw new Exception(tr('user_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method users_status
     *
     * @uses To update user status as DECLINED/APPROVED based on users id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - User Id
     * 
     * @return response success/failure message
     *
     **/
    public function users_status(Request $request) {

        try {

            DB::beginTransaction();

            $user_details = User::find($request->user_id);

            if(!$user_details) {

                throw new Exception(tr('user_not_found'), 101);
                
            }

            $user_details->status = $user_details->status ? DECLINED : APPROVED ;

            if($user_details->save()) {

                DB::commit();

                $message = $user_details->status ? tr('user_approve_success') : tr('user_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.users.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method users_verify_status()
     *
     * @uses verify the user
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - User Id
     *
     * @return redirect back page with status of the user verification
     */
    public function users_verify_status(Request $request) {

        try {

            DB::beginTransaction();

            $user_details = User::find($request->user_id);

            if(!$user_details) {

                throw new Exception(tr('user_details_not_found'), 101);
                
            }

            $user_details->is_verified = $user_details->is_verified ? USER_EMAIL_NOT_VERIFIED : USER_EMAIL_VERIFIED;

            if($user_details->save()) {

                DB::commit();

                $message = $user_details->is_verified ? tr('user_verify_success') : tr('user_unverify_success');

                return redirect()->route('admin.users.index')->with('flash_success', $message);
            }
            
            throw new Exception(tr('user_verify_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.users.index')->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method users_subscriptions_index()
     *
     * @uses Used to display all subscription available for all users
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - subscripiton id
     *
     * @return redirect back page with status of the user verification
     */
    public function users_subscriptions_index(Request $request) 
    {
        $subscriptions = Subscription::where('status',APPROVED)->get();
        
        $user_details = User::where('id',$request->user_id)->first();

        $subscripton_payment_history = SubscriptionPayment::where('user_id',$request->user_id)->paginate($this->take);
        
        return view('admin.users.subscriptions.index')
                    ->with('main_page','users-crud')
                    ->with('page','users')
                    ->with('sub_page','users-view')
                    ->with('subscriptions',$subscriptions)
                    ->with('user_details',$user_details)
                    ->with('subscripton_payment_history',$subscripton_payment_history);
    }

    /**
     * @method users_subscription_payments_save()
     *
     * @uses Used to subscribe perticulor subscription 
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - subscripiton id
     *
     * @return redirect back page with status of the user verification
     */
    public function users_subscription_payments_save(Request $request) 
    {
        try {
           
            DB::begintransaction();
            
            $rules = [
                'subscription_id' =>'required|exists:subscriptions,id',
                'user_id' => 'required|exists:users,id'
            ];

            Helper::custom_validator($request->all(),$rules);

            $subscription_details = Subscription::where('id',$request->subscription_id)->first();

            \App\SubscriptionPayment::where('user_id', $request->user_id)->where('is_current_subscription', YES)->update(['is_current_subscription' => NO]);

            $previous_payment = \App\SubscriptionPayment::where('user_id', $request->user_id)
                                            ->where('status', PAID)
                                            ->orderBy('created_at', 'desc')
                                            ->first();

            $subscription_payment = new \App\SubscriptionPayment;

            $plan_type = $subscription_details->plan_type ?? PLAN_TYPE_MONTH; // For future purpose, dont remove

            $subscription_payment->expiry_date = date('Y-m-d H:i:s',strtotime("+{$subscription_details->plan} {$plan_type}"));


            if($previous_payment) {

                if (strtotime($previous_payment->expiry_date) >= strtotime(date('Y-m-d H:i:s'))) {
                    $subscription_payment->expiry_date = date('Y-m-d H:i:s', strtotime("+{$subscription_details->plan} {$plan_type}", strtotime($previous_payment->expiry_date)));
                }
            }

            $subscription_payment->subscription_id = $request->subscription_id;

            $subscription_payment->user_id = $request->user_id;

            $subscription_payment->payment_id = 'A-PAID-'.rand(1, 999999);

            $subscription_payment->status = PAID;

            $subscription_payment->amount = $subscription_details->amount ?? 0.00;

            $subscription_payment->payment_mode = COD;

            $subscription_payment->is_current_subscription = YES;

            $subscription_payment->no_of_users = $subscription_details->no_of_users;

            $subscription_payment->no_of_minutes = $subscription_details->no_of_minutes;

            $subscription_payment->plan = $subscription_details->plan;

            $subscription_payment->plan_type = $subscription_details->plan_type;

            $subscription_payment->cancel_reason = "";
            
            $subscription_payment->paid_date = date('Y-m-d H:i:s');

            if($subscription_payment->save()) {

                DB::commit();

                return redirect()->back()->with('flash_success',tr('subscription_payment_success'));
                
            }

            throw new Exception(tr('subscription_payment_failed'), 101);

        } catch(Exception $e){ 

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());

        } 
    }

    /**
     * @method users_subscription_payments_delete()
     *
     * @uses Used to delete the payment record based on payment Id
     *
     * @created Akshata
     *
     * @updated
     *
     * @param object $request - subsctiption payment Id
     *
     * @return 
     */
    public function users_subscription_payments_delete(Request $request) {

        try {

            DB::begintransaction();

            $subscripton_payment_details = SubscriptionPayment::find($request->subscription_payment_id);
            
            if(!$subscripton_payment_details) {

                throw new Exception(tr('subscription_payment_details_not_found'), 101);                
            }

            if($subscripton_payment_details->delete()) {

                DB::commit();

                return redirect()->back()->with('flash_success',tr('subscription_payment_deleted_success'));   

            } 
            
            throw new Exception(tr('subscription_payment_failed_to_delete'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }     
    
    }


    /**
     * @method meetings_index()
     *
     * @uses To list out meetings details 
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function meetings_index(Request $request) {

        $base_query = Meeting::orderBy('meetings.id','desc');

        if($request->meeting_type) {

            switch ($request->meeting_type) {

                case SORT_BY_MEETING_UPCOMING:
                    $base_query = $base_query->where('meeting_type',MEETING_TYPE_UPCOMING);
                    break;

                case SORT_BY_MEETING_ONLIVE:
                    $base_query = $base_query->where('meeting_type',MEETING_TYPE_LIVE);
                    break;

                case SORT_BY_MEETING_ENDED:
                    $base_query = $base_query->where('meeting_type',MEETING_TYPE_ENDED);
                    break;
                
                default:
                    $base_query = $base_query->where('meeting_type',MEETING_TYPE_CANCELLED);
                    break;
            }
        }

        if($request->type) {

            $base_query = $base_query->where('status',MEETING_SCHEDULED);
            
            $sub_page = 'meetings-scheduled';
        }

        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query =  $base_query->whereHas('userDetails',function($query) use($search_key) {

                return $query->where('users.name','LIKE','%'.$search_key.'%');

            })->orWhere('meetings.title','LIKE','%'.$search_key.'%');
                        
        }

        if($request->status) {

            $base_query = $base_query->where('status',$request->status);
            
        }

        $sub_page = isset($sub_page) ? $sub_page : "meetings-index";
         
        $meetings = $base_query->paginate($this->take);

        foreach ($meetings as $key => $meeting_details) {
            
            $meeting_details->users_count = $meeting_details->meetingUsers ? $meeting_details->meetingUsers->count() : 0;
        }

        return view('admin.meetings.index')
                    ->with('main_page','meetings-crud')
                    ->with('page', 'meetings')
                    ->with('sub_page', $sub_page)
                    ->with('meetings', $meetings);
    }

    /**
     * @method meetings_create()
     *
     * @uses To create meetings details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function meetings_create() {

        $meeting_details = new Meeting;

        $users = User::where('status',APPROVED)->get();

        return view('admin.meetings.create')
                    ->with('main_page','meetings-crud')
                    ->with('page' , 'meetings')
                    ->with('sub_page','meetings-create')
                    ->with('meeting_details', $meeting_details)
                    ->with('users',$users);
    }

    /**
     * @method meetings_edit()
     *
     * @uses To display and update meetings details based on the meeting id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Room Id
     * 
     * @return redirect view page 
     *
     */
    public function meetings_edit(Request $request) {

        try {

            $meeting_details = Meeting::find($request->meeting_id);

            $users = User::where('status',APPROVED)->get();

            $users  = selected($users,$meeting_details->user_id,'id');

            if(!$meeting_details) { 

                throw new Exception(tr('meeting_not_found'), 101);
            }

            return view('admin.meetings.edit')
                    ->with('main_page','meetings-crud')
                    ->with('page' , 'meetings')
                    ->with('sub_page','meetings-index')
                    ->with('meeting_details' , $meeting_details)
                    ->with('users',$users);
            
        } catch(Exception $e) {

            return redirect()->route('admin.meetings.index')->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method meetings_save()
     *
     * @uses To save the meetings details of new/existing meeting object based on details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object request - meeting Form Data
     *
     * @return success message
     *
     */
    public function meetings_save(Request $request) {

        try {
           
            DB::begintransaction();

            $rules = [
                'title'  => 'required|max:255',
                'description' => 'max:255',
                'picture' => 'mimes:jpg,png,jpeg',
                'start_time' =>'required',
                'end_time' => 'required',
                'user_ids' => $request->meeting_id ? '' : 'required',
            ];

            Helper::custom_validator($request->all(),$rules);

            $meeting_details = $request->meeting_id ? Meeting::find($request->meeting_id) : new Meeting;

            if(!$meeting_details) {

                throw new Exception(tr('meeting_not_found'), 101);
            }

            $meeting_details->unique_id =  uniqid();

            $meeting_details->created_by = ADMIN;

            $meeting_details->status = APPROVED;

            $meeting_details->user_id = $request->user_id ?: 0;

            $meeting_details->title = $request->title;

            $meeting_details->schedule_time = isset($request->schedule_time) ? convertTimeToUTCzone(date('Y-m-d H:i:s', strtotime($request->schedule_time)),Auth::guard('admin')->user()->timezone) : NULL;

            $meeting_details->start_time = convertTimeToUTCzone(date('Y-m-d H:i:s', strtotime($request->start_time)),Auth::guard('admin')->user()->timezone);

            $meeting_details->end_time = convertTimeToUTCzone(date('Y-m-d H:i:s', strtotime($request->end_time)),Auth::guard('admin')->user()->timezone);

            $meeting_details->description = $request->description ?: "";

            if($request->hasFile('picture')) {

                if($request->meeting_id) {

                    Helper::storage_delete_file($meeting_details->picture, COMMON_FILE_PATH); 
                    // Delete the old pic
                }

                $meeting_details->picture = Helper::storage_upload_file($request->file('picture'), COMMON_FILE_PATH);
            }

           
            if( $meeting_details->save() ) {

                if(!$request->meeting_id) {

                    foreach ($request->user_ids as $user_id)
                    {
                        $check_meeting_user = MeetingUser::where('meeting_id',$meeting_details->id)->where('user_id',$user_id)->first();

                        if($check_meeting_user) {

                            throw new Exception(tr('meeting_user_already_present'), 1);
                        }

                        $user_details = User::where('id',$user_id)->first();

                        if(!$user_details) {

                            throw new Exception(tr('user_details_not_found'), 101);
                            
                        }

                        $meeting_user_details = new MeetingUser;
                        
                        $meeting_user_details->user_id = $user_id;

                        $meeting_user_details->username = $user_details->name;

                        $meeting_user_details->meeting_id = $meeting_details->id;

                        $meeting_user_details->start_time = convertTimeToUTCzone(date('Y-m-d H:i:s', strtotime($request->start_time)),Auth::guard('admin')->user()->timezone);

                        $meeting_user_details->end_time = convertTimeToUTCzone(date('Y-m-d H:i:s', strtotime($request->end_time)),Auth::guard('admin')->user()->timezone);

                        $meeting_user_details->save();

                        DB::commit();
                    }
                        
                }

                DB::commit();

                $message = $request->meeting_id ? tr('meeting_update_success')  : tr('meeting_create_success');

                return redirect()->route('admin.meetings.view', ['meeting_id' => $meeting_details->id])->with('flash_success', $message);
            } 

            throw new Exception(tr('meeting_saved_error') , 101);

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        } 

    }

    /**
     * @method meetings_view()
     *
     * @uses view the meetings details based on meetings id
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - meeting Id
     * 
     * @return View page
     *
     */
    public function meetings_view(Request $request) {
       
        try {
    
            $meeting_details = Meeting::find($request->meeting_id);

            $meeting_details->users_count = $meeting_details->meetingUsers()->count() ?? 0;
            
            if(!$meeting_details) { 

                throw new Exception(tr('meeting_not_found'), 101);                
            }

            return view('admin.meetings.view')
                        ->with('main_page','meetings-crud')
                        ->with('page', 'meetings') 
                        ->with('sub_page','meetings-index') 
                        ->with('meeting_details' , $meeting_details);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method meetings_delete()
     *
     * @uses delete the meetings details based on meeting id
     *
     * @created Akshata 
     *
     * @updated  
     *
     * @param object $request - Meeting Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function meetings_delete(Request $request) {

        try {

            DB::begintransaction();

            $meeting_details = Meeting::find($request->meeting_id);
            
            if(!$meeting_details) {

                throw new Exception(tr('meeting_not_found'), 101);                
            }

            if($meeting_details->delete()) {

                DB::commit();

                return redirect()->route('admin.meetings.index')->with('flash_success',tr('meeting_deleted_success'));   

            } 
            
            throw new Exception(tr('meeting_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method meeting_status
     *
     * @uses To update meeting status as DECLINED/APPROVED based on meeting id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Meeting Id
     * 
     * @return response success/failure message
     *
     **/
    public function meetings_status(Request $request) {

        try {

            DB::beginTransaction();

            $meeting_details = Meeting::find($request->meeting_id);

            if(!$meeting_details) {

                throw new Exception(tr('meeting_not_found'), 101);
                
            }

            $meeting_details->status = $meeting_details->status ? DECLINED : APPROVED ;

            if($meeting_details->save()) {

                DB::commit();

                $message = $meeting_details->status ? tr('meeting_approve_success') : tr('meeting_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('meeting_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.meetings.index')->with('flash_error', $e->getMessage());

        }

    }

    /**
     * @method meeting_recorded_videos()
     *
     * @uses To lists all record video lists
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function meeting_recorded_videos(Request $request) {

        $meeting_recorded_videos = MeetingRecord::paginate($this->take);
        
        return view('admin.recorded_videos.index')
                ->with('main_page','record-videos')
                ->with('page','meeting-record-videos')
                ->with('sub_page','meeting-record-videos-lists')
                ->with('meeting_recorded_videos',$meeting_recorded_videos);

    }

    /**
     * @method meeting_recorded_videos_view()
     *
     * @uses To lists all record video lists
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function meeting_recorded_videos_view(Request $request) {

        try {

            $meeting_record_video_details = MeetingRecord::where('id',$request->video_id)->first();
           
            if(!$meeting_record_video_details) {

                throw new Exception(tr('meeting_record_video_not_found'), 101);
                
            }

            return view('admin.recorded_videos.view')
                ->with('main_page','record-videos')
                ->with('page','meeting-record-videos')
                ->with('sub_page','meeting-record-videos-lists')
                ->with('meeting_record_video_details',$meeting_record_video_details); 

        } catch (Exception $e) {
            
            return redirect()->route('admin.meetings.recorded_videos')->with('flash_error', $e->getMessage());
        }

    }

    /**
     * @method subscriptions_index()
     *
     * @uses To list out subscription details 
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function subscriptions_index() {

        $subscriptions = Subscription::orderBy('updated_at','desc')->paginate($this->take);

        return view('admin.subscriptions.index')
                    ->with('main_page','subscriptions-crud')
                    ->with('page','subscriptions')
                    ->with('sub_page' , 'subscriptions-view')
                    ->with('subscriptions' , $subscriptions);
    }

    /**
     * @method subscriptions_create()
     *
     * @uses To create subscriptions details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function subscriptions_create() {

        $subscription_details = new Subscription;

        $subscription_plan_types = [PLAN_TYPE_MONTH,PLAN_TYPE_YEAR,PLAN_TYPE_WEEK,PLAN_TYPE_DAY];

        return view('admin.subscriptions.create')
                    ->with('main_page','subscriptions-crud')
                    ->with('page' , 'subscriptions')
                    ->with('sub_page','subscriptions-create')
                    ->with('subscription_details', $subscription_details)
                    ->with('subscription_plan_types',$subscription_plan_types);           
    }

    /**
     * @method subscriptions_edit()
     *
     * @uses To display and update subscriptions details based on the subscription id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Subscription Id
     * 
     * @return redirect view page 
     *
     */
    public function subscriptions_edit(Request $request) {

        try {

            $subscription_details = Subscription::find($request->subscription_id);

            if(!$subscription_details) { 

                throw new Exception(tr('subscription_not_found'), 101);
            }

            $subscription_plan_types = [PLAN_TYPE_MONTH,PLAN_TYPE_YEAR,PLAN_TYPE_WEEK,PLAN_TYPE_DAY];
           
            return view('admin.subscriptions.edit')
                    ->with('main_page','subscriptions-crud')
                    ->with('page' , 'subscriptions')
                    ->with('sub_page','subscriptions-view')
                    ->with('subscription_details' , $subscription_details)
                    ->with('subscription_plan_types',$subscription_plan_types); 
            
        } catch(Exception $e) {

            return redirect()->route('admin.subscriptions.index')->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method subscriptions_save()
     *
     * @uses To save the subscriptions details of new/existing subscription object based on details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object request - Subscrition Form Data
     *
     * @return success message
     *
     */
    public function subscriptions_save(Request $request) {

        try {
           
            DB::begintransaction();

            $rules = [
                'title'  => 'required|max:255',
                'description' => 'max:255',
                'amount' => 'required|numeric|min:0|max:10000000',
                'plan' => 'required',
                'plan_type' => 'required',
                'no_of_users' => 'required|min:1',
                'no_of_minutes' => 'required|min:1',
            ];

            Helper::custom_validator($request->all(),$rules);

            $subscription_details = $request->subscription_id ? Subscription::find($request->subscription_id) : new Subscription;

            if(!$subscription_details) {

                throw new Exception(tr('subscription_not_found'), 101);
            }

            $subscription_details->status = APPROVED;

            $subscription_details->title = $request->title;

            $subscription_details->description = $request->description ?: "";

            $subscription_details->plan = $request->plan;

            $subscription_details->plan_type = $request->plan_type;

            $subscription_details->amount = $request->amount;

            $subscription_details->is_free = $request->amount <= 0 ? YES : NO;
        
            $subscription_details->is_popular  = $request->is_popular ?? NO;

            $subscription_details->no_of_users  = $request->no_of_users ?? 1;

            $subscription_details->no_of_minutes  = $request->no_of_minutes ?? 1;

            if( $subscription_details->save() ) {

                DB::commit();

                $message = $request->subscription_id ? tr('subscription_update_success')  : tr('subscription_create_success');

                return redirect()->route('admin.subscriptions.view', ['subscription_id' => $subscription_details->id])->with('flash_success', $message);
            } 

            throw new Exception(tr('subscription_saved_error') , 101);

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error', $e->getMessage());
        } 

    }

    /**
     * @method subscriptions_view()
     *
     * @uses view the subscriptions details based on subscriptions id
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - Subscription Id
     * 
     * @return View page
     *
     */
    public function subscriptions_view(Request $request) {
       
        try {
      
            $subscription_details = Subscription::find($request->subscription_id);
            
            if(!$subscription_details) { 

                throw new Exception(tr('subscription_not_found'), 101);                
            }

            return view('admin.subscriptions.view')
                        ->with('main_page','subscriptions-crud')
                        ->with('page', 'subscriptions') 
                        ->with('sub_page','subscriptions-view') 
                        ->with('subscription_details' , $subscription_details);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
    
    }

    /**
     * @method subscriptions_delete()
     *
     * @uses delete the subscription details based on subscription id
     *
     * @created Akshata 
     *
     * @updated  
     *
     * @param object $request - Subscription Id
     * 
     * @return response of success/failure details with view page
     *
     */
    public function subscriptions_delete(Request $request) {

        try {

            DB::begintransaction();

            $subscription_details = Subscription::find($request->subscription_id);
            
            if(!$subscription_details) {

                throw new Exception(tr('subscription_not_found'), 101);                
            }

            if($subscription_details->delete()) {

                DB::commit();

                return redirect()->route('admin.subscriptions.index')->with('flash_success',tr('subscription_deleted_success'));   

            } 
            
            throw new Exception(tr('subscription_delete_failed'));
            
        } catch(Exception $e){

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }       
         
    }

    /**
     * @method subscriptions_status
     *
     * @uses To update subscription status as DECLINED/APPROVED based on subscriptions id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - Subscription Id
     * 
     * @return response success/failure message
     *
     **/
    public function subscriptions_status(Request $request) {

        try {

            DB::beginTransaction();

            $subscription_details = Subscription::find($request->subscription_id);

            if(!$subscription_details) {

                throw new Exception(tr('subscription_not_found'), 101);
                
            }

            $subscription_details->status = $subscription_details->status ? DECLINED : APPROVED ;

            if($subscription_details->save()) {

                DB::commit();

                $message = $subscription_details->status ? tr('subscription_approve_success') : tr('subscription_decline_success');

                return redirect()->back()->with('flash_success', $message);
            }
            
            throw new Exception(tr('subscription_status_change_failed'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.subscriptions.index')->with('flash_error', $e->getMessage());

        }

    }

     /**
     * @method subscription_payments_index()
     *
     * @uses To create subscriptions details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function subscription_payments_index(Request $request) {

        $base_query = SubscriptionPayment::orderBy('subscription_payments.id','desc');

        if($request->today_revenue) {

            $base_query = $base_query->whereDate('subscription_payments.created_at',today());
        }

        if ($request->subscription_id) {

            $base_query = $base_query->where('subscription_payments.subscription_id',$request->subscription_id);
            
        }
        
        if($request->search_key) {

            $search_key = $request->search_key;

            $base_query =  $base_query
                            ->orWhereHas('userDetails', function($q) use ($search_key) {

                                return $q->where('users.name','LIKE','%'.$search_key.'%');

                            })->orWhereHas('subscriptionDetails', function($q) use ($search_key) {

                                return $q->where('subscriptions.title','LIKE','%'.$search_key.'%');
                            })
                            ->orWhere('subscription_payments.payment_id','LIKE','%'.$search_key.'%');

        }

        $subscription_payments = $base_query->paginate($this->take);

        return view('admin.payments.subscription_payments.index')
                    ->with('main_page','payments-crud')
                    ->with('page' , 'payments')
                    ->with('sub_page','subscription-payments')
                    ->with('subscription_payments', $subscription_payments);           
    }

    /**
     * @method subscription_payments_view()
     *
     * @uses display the secified subscription details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function subscription_payments_view(Request $request) {

        try {

            $subscription_payment_details = SubscriptionPayment::where('subscription_payments.id', $request->subscription_payment_id)->first();

            if(!$subscription_payment_details) { 

                throw new Exception(tr('subscription_payment_not_found'), 101);                
            }

            return view('admin.payments.subscription_payments.view')
                    ->with('main_page','payments-crud')
                    ->with('page' , 'payments')
                    ->with('sub_page','subscription-payments')
                    ->with('subscription_payment_details', $subscription_payment_details);
            
        } catch (Exception $e) {

            return redirect()->back()->with('flash_error', $e->getMessage());
        }
                   
    }
    /**
     * @method revenue_dashboard()
     *
     * @uses Display revenue details
     *
     * @created  Akshata
     *
     * @updated 
     *
     * @param 
     * 
     * @return return view page
     *
     */
    public function revenue_dashboard() {

        $data['total_subscribers'] = SubscriptionPayment::count('user_id');
        
        $data['today_subscribers'] = SubscriptionPayment::whereDate('created_at',today())->count();

        $data['total_earnings'] = SubscriptionPayment::sum('amount');

        $data['today_earnings'] = SubscriptionPayment::whereDate('created_at',today())->sum('amount');

        $data = (object) $data;

        $data->analytics = last_x_days_revenue(7);

        return view('admin.payments.revenue_dashboard')
                ->with('main_page','payments-crud')
                ->with('page', 'payments')
                ->with('sub_page' ,'revenues-dashboard')
                ->with('data', $data);

    }

    

    /**
     * @method settings()
     *
     * @uses  Used to display the setting page
     *
     * @created Akshata
     *
     * @updated
     *
     * @param 
     *
     * @return view page 
     */

    public function settings() {

        $env_values = EnvEditorHelper::getEnvValues();

        return view('admin.settings.settings')
                ->with('env_values',$env_values)
                ->with('main_page' , 'settings');
    }
    
    /**
     * @method settings_save()
     * 
     * @uses to update settings details
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param (request) setting details
     *
     * @return success/error message
     */
    public function settings_save(Request $request) {
       
        try {
            
            DB::beginTransaction();
            
            $rules =  
                [
                    'site_logo' => 'mimes:jpeg,jpg,bmp,png',
                    'site_icon' => 'mimes:jpeg,jpg,bmp,png',
                ];

            $custom_errors = 
                [
                    'mimes' => tr('image_error')
                ];

            Helper::custom_validator($request->all(),$rules,$custom_errors);

            foreach( $request->toArray() as $key => $value) {

                if($key != '_token') {

                    $check_settings = Settings::where('key' ,'=', $key)->count();

                    if( $check_settings == 0 ) {

                        throw new Exception( $key.tr('settings_key_not_found'), 101);
                    }
                    
                    if( $request->hasFile($key) ) {
                                            
                        $file = Settings::where('key' ,'=', $key)->first();
                       
                        Helper::storage_delete_file($file->value, FILE_PATH_SITE);

                        $file_path = Helper::storage_upload_file($request->file($key) , FILE_PATH_SITE);    

                        $result = Settings::where('key' ,'=', $key)->update(['value' => $file_path]); 

                        if( $result == TRUE ) {
                     
                            DB::commit();
                   
                        } else {

                            throw new Exception(tr('settings_save_error'), 101);
                        } 
                   
                    } else {

                        if(isset($value)) {

                            $result = Settings::where('key' ,'=', $key)->update(['value' => $value]);

                        } else {

                            $result = Settings::where('key' ,'=', $key)->update(['value' => '']);
                        } 
                        
                        if( $result == TRUE ) {
                         
                            DB::commit();
                       
                        } else {

                            throw new Exception(tr('settings_save_error'), 101);
                        } 

                    }  
 
                }
            }

            Helper::settings_generate_json();

            return back()->with('flash_success', tr('settings_update_success'));
            
        } catch (Exception $e) {

            DB::rollback();

            return back()->with('flash_error', $e->getMessage());
        
        }
    }

    /**
     * @method env_settings_save()
     *
     * @uses To update the email details for .env file
     *
     * @created Akshata
     *
     * @updated
     *
     * @param Form data
     *
     * @return view page
     */

    public function env_settings_save(Request $request) {

        try {

            $env_values = EnvEditorHelper::getEnvValues();

            $env_settings = ['MAIL_DRIVER' , 'MAIL_HOST' , 'MAIL_PORT' , 'MAIL_USERNAME' , 'MAIL_PASSWORD' , 'MAIL_ENCRYPTION' , 'MAILGUN_DOMAIN' , 'MAILGUN_SECRET' , 'FCM_SERVER_KEY', 'FCM_SENDER_ID' , 'FCM_PROTOCOL'];

            if($env_values) {

                foreach ($env_values as $key => $data) {

                    if($request->$key) { 

                        \Enveditor::set($key, $request->$key);

                    }
                }
            }

            $message = tr('settings_update_success');

            return redirect()->route('clear-cache')->with('flash_success', $message);  

        } catch(Exception $e) {

            return back()->withInput()->with('flash_error' , $e->getMessage());

        }  

    }

    /**
     * @method profile()
     *
     * @uses  Used to display the logged in admin details
     *
     * @created Akshata
     *
     * @updated
     *
     * @param 
     *
     * @return view page 
     */

    public function profile() {

        return view('admin.account.profile')->with('main_page', 'profile');
    }

    /**
     * @method profile_save()
     *
     * @uses To update the admin details
     *
     * @created Akshata
     *
     * @updated
     *
     * @param -
     *
     * @return view page 
     */

    public function profile_save(Request $request) {

        try {

            DB::beginTransaction();

            $rules = 
                [
                    'name' => 'max:191',
                    'email' => $request->admin_id ? 'email|email:rfc,dns|max:191|unique:admins,email,'.$request->admin_id : 'email|email:rfc,dns|max:191|unique:admins,email,NULL',
                    'admin_id' => 'required|exists:admins,id',
                    'picture' => 'mimes:jpeg,jpg,png'
                ];
            
            Helper::custom_validator($request->all(),$rules);
            
            $admin_details = Admin::find($request->admin_id);

            if(!$admin_details) {

                Auth::guard('admin')->logout();

                throw new Exception(tr('admin_details_not_found'), 101);
            }
        
            $admin_details->name = $request->name ?: $admin_details->name;

            $admin_details->email = $request->email ?: $admin_details->email;

            if($request->hasFile('picture') ) {
                
                Helper::storage_delete_file($admin_details->picture, PROFILE_PATH_ADMIN); 
                
                $admin_details->picture = Helper::storage_upload_file($request->file('picture'), PROFILE_PATH_ADMIN);
            }
            
            $admin_details->remember_token = Helper::generate_token();

            $admin_details->save();

            DB::commit();

            return redirect()->route('admin.profile')->with('flash_success', tr('admin_profile_success'));


        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error' , $e->getMessage());

        }    
    
    }

    /**
     * @method change_password()
     *
     * @uses To change the admin password
     *
     * @created Akshata
     *
     * @updated
     *
     * @param 
     *
     * @return view page 
     */

    public function change_password(Request $request) {

        try {

            DB::begintransaction();

            $rules = 
            [              
                'password' => 'required|confirmed|min:6',
                'old_password' => 'required',
            ];
            
            Helper::custom_validator($request->all(),$rules);

            $admin_details = Admin::find(Auth::guard('admin')->user()->id);

            if(!$admin_details) {

                Auth::guard('admin')->logout();
                              
                throw new Exception(tr('admin_details_not_found'), 101);

            }

            if(Hash::check($request->old_password,$admin_details->password)) {

                $admin_details->password = Hash::make($request->password);

                $admin_details->save();

                DB::commit();

                Auth::guard('admin')->logout();

                return redirect()->route('admin.login')->with('flash_success', tr('password_change_success'));
                
            } else {

                throw new Exception(tr('password_mismatch'));
            }

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->withInput()->with('flash_error' , $e->getMessage());

        }    
    
    }

    /**
     * @method static_pages_index()
     *
     * @uses To list the static pages
     *
     * @created Akshata
     *
     * @updated   
     *
     * @param -
     *
     * @return List of pages   
     */

    public function static_pages_index() {

        $static_pages = StaticPage::orderBy('updated_at' , 'desc')->paginate($this->take);

        return view('admin.static_pages.index')
                    ->with('main_page','static_pages-crud')
                    ->with('page','static_pages')
                    ->with('sub_page',"static_pages-view")
                    ->with('static_pages',$static_pages);
    
    }

    /**
     * @method static_pages_create()
     *
     * @uses To create static_page details
     *
     * @created Akshata
     *
     * @updated    
     *
     * @param
     *
     * @return view page   
     *
     */
    public function static_pages_create() {

        $static_keys = ['about' , 'contact' , 'privacy' , 'terms' , 'help' , 'faq' , 'refund', 'cancellation'];

        foreach ($static_keys as $key => $static_key) {

            // Check the record exists

            $check_page = StaticPage::where('type', $static_key)->first();

            if($check_page) {
                unset($static_keys[$key]);
            }
        }

        $section_types = static_page_footers(0, $is_list = YES);

        $static_keys[] = 'others';

        $static_page_details = new StaticPage;

        return view('admin.static_pages.create')
                ->with('main_page','static_pages-crud')
                ->with('page','static_pages')
                ->with('sub_page',"static_pages-create")
                ->with('static_keys', $static_keys)
                ->with('static_page_details',$static_page_details)
                ->with('section_types',$section_types);
   
    }

    /**
     * @method static_pages_edit()
     *
     * @uses To display and update static_page details based on the static_page id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param object $request - static_page Id
     * 
     * @return redirect view page 
     *
     */
    public function static_pages_edit(Request $request) {

        try {

            $static_page_details = StaticPage::find($request->static_page_id);

            if(!$static_page_details) {

                throw new Exception(tr('static_page_not_found'), 101);
            }

            $static_keys = ['about' , 'contact' , 'privacy' , 'terms' , 'help' , 'faq' , 'refund', 'cancellation'];

            foreach ($static_keys as $key => $static_key) {

                // Check the record exists

                $check_page = StaticPage::where('type', $static_key)->first();

                if($check_page) {
                    unset($static_keys[$key]);
                }
            }

            $section_types = static_page_footers(0, $is_list = YES);

            $static_keys[] = 'others';

            $static_keys[] = $static_page_details->type;

            return view('admin.static_pages.edit')
                    ->with('main_page','static_pages-crud')
                    ->with('page' , 'static_pages')
                    ->with('sub_page','static_pages-view')
                    ->with('static_keys' , array_unique($static_keys))
                    ->with('static_page_details' , $static_page_details)
                    ->with('section_types',$section_types);
            
        } catch(Exception $e) {

            return redirect()->route('admin.static_pages.index')->with('flash_error' , $error);

        }
    }

    /**
     * @method static_pages_save()
     *
     * @uses To create/update the page details 
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param
     *
     * @return index page    
     *
     */
    public function static_pages_save(Request $request) {

        try {

            DB::beginTransaction();

            $rules = [
                    'title' => 'required|max:191',
                    'description' => 'required',
                    'type' => !$request->static_page_id ? 'required' : ""
                ];
                   
            Helper::custom_validator($request->all(),$rules);

            if($request->static_page_id != '') {

                $static_page_details = StaticPage::find($request->static_page_id);

                $message = tr('static_page_updated_success');                    

            } else {

                $check_page = "";

                // Check the staic page already exists                  
                
                if($request->type != 'others') {

                    $check_page = StaticPage::where('type',$request->type)->first();

                    if($check_page) {

                        return back()->with('flash_error',tr('static_page_already_alert'));
                    }

                }

                $message = tr('static_page_created_success');

                $static_page_details = new StaticPage;

                $static_page_details->status = APPROVED;

            }

            $static_page_details->title = $request->title ?: $static_page_details->title;

            $static_page_details->description = $request->description ?: $static_page_details->description;

            $static_page_details->type = $request->type ?: $static_page_details->type;

            $static_page_details->section_type = $request->section_type ?: $static_page_details->section_type;

            $unique_id = $request->type ?: $static_page_details->type;

            // Dont change the below code. If any issue, get approval from vithya and change

            if(!in_array($unique_id, ['about', 'privacy', 'terms', 'contact', 'help', 'faq'])) {

                $unique_id = routefreestring($request->title ?? rand());

                $unique_id = in_array($unique_id, ['about', 'privacy', 'terms', 'contact', 'help', 'faq']) ? $unique_id : $unique_id;

            }

            $static_page_details->unique_id = $unique_id ?? rand();

            if($static_page_details->save()) {

                DB::commit();

                Helper::settings_generate_json();
                
                return redirect()->route('admin.static_pages.view', ['static_page_id' => $static_page_details->id] )->with('flash_success', $message);

            } 

            throw new Exception(tr('static_page_save_failed'), 101);
                      
        } catch(Exception $e) {

            DB::rollback();

            return back()->withInput()->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method static_pages_delete()
     *
     * Used to view file of the create the static page 
     *
     * @created Akshata
     *
     * @updated  
     *
     * @param -
     *
     * @return view page   
     */

    public function static_pages_delete(Request $request) {

        try {

            DB::beginTransaction();

            $static_page_details = StaticPage::find($request->static_page_id);

            if(!$static_page_details) {

                throw new Exception(tr('static_page_not_found'), 101);
                
            }

            if($static_page_details->delete()) {

                DB::commit();

                return redirect()->route('admin.static_pages.index')->with('flash_success',tr('static_page_deleted_success')); 

            } 

            throw new Exception(tr('static_page_error'));

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->route('admin.static_pages.index')->with('flash_error', $e->getMessage());

        }
    
    }

    /**
     * @method static_pages_view()
     *
     * @uses view the static_pages details based on static_pages id
     *
     * @created Akshata 
     *
     * @updated 
     *
     * @param object $request - static_page Id
     * 
     * @return View page
     *
     */
    public function static_pages_view(Request $request) {

        $static_page_details = StaticPage::find($request->static_page_id);

        if(!$static_page_details) {
           
            return redirect()->route('admin.static_pages.index')->with('flash_error',tr('static_page_not_found'));

        }

        return view('admin.static_pages.view')
                    ->with('main_page','static_pages-crud')
                    ->with('page', 'static_pages')
                    ->with('sub_page','static_pages-view')
                    ->with('static_page_details' , $static_page_details);
    }

    /**
     * @method static_pages_status_change()
     *
     * @uses To update static_page status as DECLINED/APPROVED based on static_page id
     *
     * @created Akshata
     *
     * @updated 
     *
     * @param - integer static_page_id
     *
     * @return view page 
     */

    public function static_pages_status_change(Request $request) {

        try {

            DB::beginTransaction();

            $static_page_details = StaticPage::find($request->static_page_id);

            if(!$static_page_details) {

                throw new Exception(tr('static_page_not_found'), 101);
                
            }

            $static_page_details->status = $static_page_details->status == DECLINED ? APPROVED : DECLINED;

            $static_page_details->save();

            DB::commit();

            $message = $static_page_details->status == DECLINED ? tr('static_page_decline_success') : tr('static_page_approve_success');

            return redirect()->back()->with('flash_success', $message);

        } catch(Exception $e) {

            DB::rollback();

            return redirect()->back()->with('flash_error', $e->getMessage());

        }

    }

}
