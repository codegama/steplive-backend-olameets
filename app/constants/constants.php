<?php

/*
|--------------------------------------------------------------------------
| Application Constants
|--------------------------------------------------------------------------
|
| 
|
*/

if(!defined('TAKE_COUNT')) define('TAKE_COUNT', 6);

if(!defined('NO')) define('NO', 0);

if(!defined('YES')) define('YES', 1);

if(!defined('PAID')) define('PAID',1);

if(!defined('UNPAID')) define('UNPAID', 0);

if(!defined('DEVICE_ANDROID')) define('DEVICE_ANDROID', 'android');

if(!defined('DEVICE_IOS')) define('DEVICE_IOS', 'ios');

if(!defined('DEVICE_WEB')) define('DEVICE_WEB', 'web');

if(!defined('APPROVED')) define('APPROVED', 1);

if(!defined('DECLINED')) define('DECLINED', 0);

if(!defined('DEFAULT_TRUE')) define('DEFAULT_TRUE', true);

if(!defined('DEFAULT_FALSE')) define('DEFAULT_FALSE', false);

if(!defined('ADMIN')) define('ADMIN', 'admin');

if(!defined('USER')) define('USER', 'user');

if(!defined('COD')) define('COD',   'COD');

if(!defined('PAYPAL')) define('PAYPAL', 'PAYPAL');

if(!defined('CARD')) define('CARD',  'CARD');

if(!defined('NULL')) define('NULL',  null);

if(!defined('STRIPE_MODE_LIVE')) define('STRIPE_MODE_LIVE',  'live');

if(!defined('STRIPE_MODE_SANDBOX')) define('STRIPE_MODE_SANDBOX',  'sandbox');

if(!defined('PAID_USER')) define('PAID_USER',  1);

//////// USERS

if(!defined('USER_PENDING')) define('USER_PENDING', 0);

if(!defined('USER_APPROVED')) define('USER_APPROVED', 1);

if(!defined('USER_DECLINED')) define('USER_DECLINED', 2);

if(!defined('USER_EMAIL_NOT_VERIFIED')) define('USER_EMAIL_NOT_VERIFIED', 0);

if(!defined('USER_EMAIL_VERIFIED')) define('USER_EMAIL_VERIFIED', 1);

//////// USERS END

/***** ADMIN CONTROLS KEYS ********/

if(!defined('ADMIN_CONTROL_ENABLED')) define('ADMIN_CONTROL_ENABLED', 1);

if(!defined('ADMIN_CONTROL_DISABLED')) define('ADMIN_CONTROL_DISABLED', 0);

if(!defined('NO_DEVICE_TOKEN')) define("NO_DEVICE_TOKEN", "NO_DEVICE_TOKEN");

if(!defined('MEETING_TYPE_LIVE')) define('MEETING_TYPE_LIVE', 'live');

if(!defined('MEETING_TYPE_VOD')) define('MEETING_TYPE_VOD', 'vod');

if(!defined('PLAN_TYPE_MONTH')) define('PLAN_TYPE_MONTH', 'months');
	
if(!defined('PLAN_TYPE_YEAR')) define('PLAN_TYPE_YEAR', 'years');

if(!defined('PLAN_TYPE_WEEK')) define('PLAN_TYPE_WEEK', 'weeks');

if(!defined('PLAN_TYPE_DAY')) define('PLAN_TYPE_DAY', 'days');


if(!defined('TODAY')) define('TODAY', 'today');

if(!defined('COMPLETED')) define('COMPLETED',3);

if(!defined('SORT_BY_APPROVED')) define('SORT_BY_APPROVED',1);

if(!defined('SORT_BY_DECLINED')) define('SORT_BY_DECLINED',2);

if(!defined('SORT_BY_EMAIL_VERIFIED')) define('SORT_BY_EMAIL_VERIFIED',3);

if(!defined('SORT_BY_EMAIL_NOT_VERIFIED')) define('SORT_BY_EMAIL_NOT_VERIFIED',4);

if(!defined('SORT_BY_MEETING_UPCOMING')) define('SORT_BY_MEETING_UPCOMING',1);

if(!defined('SORT_BY_MEETING_ONLIVE')) define('SORT_BY_MEETING_ONLIVE',2);

if(!defined('SORT_BY_MEETING_ENDED')) define('SORT_BY_MEETING_ENDED',3);

if(!defined('SORT_BY_MEETING_CANCELLED')) define('SORT_BY_MEETING_CANCELLED',4);

if(!defined('MEETING_TYPE_UPCOMING')) define('MEETING_TYPE_UPCOMING',1);

if(!defined('MEETING_TYPE_LIVE')) define('MEETING_TYPE_LIVE',2);

if(!defined('MEETING_TYPE_ENDED')) define('MEETING_TYPE_ENDED',3);

if(!defined('MEETING_TYPE_CANCELLED')) define('MEETING_TYPE_CANCELLED',4);


if(!defined('MEETING_NONE')) define('MEETING_NONE', 0);

if(!defined('MEETING_SCHEDULED')) define('MEETING_SCHEDULED', 1);

if(!defined('MEETING_STARTED')) define('MEETING_STARTED', 2);

if(!defined('MEETING_ENDED')) define('MEETING_ENDED', 3);

if(!defined('MEETING_CANCELLED')) define('MEETING_CANCELLED', 4);


if(!defined('STATIC_PAGE_SECTION_1')) define('STATIC_PAGE_SECTION_1', 1);

if(!defined('STATIC_PAGE_SECTION_2')) define('STATIC_PAGE_SECTION_2', 2);

if(!defined('STATIC_PAGE_SECTION_3')) define('STATIC_PAGE_SECTION_3', 3);

if(!defined('STATIC_PAGE_SECTION_4')) define('STATIC_PAGE_SECTION_4', 4);

