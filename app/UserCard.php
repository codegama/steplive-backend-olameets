<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCard extends Model
{
    /**
     * Get the user details associated with booking
     */
	public function subscriptionDetails() {

		return $this->belongsTo(Subscription::class);

	}
}
