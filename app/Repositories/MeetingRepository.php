<?php

namespace App\Repositories;

use App\Helpers\Helper;

use Log, Validator, Setting, Exception, DB;

class MeetingRepository {

	/**
     * @method meeetings_list_response()
     *
     * @uses 
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param
     *
     * @return 
     */

    public static function meetings_list_response($data, $request) {

        $data = $data->map(function ($value, $key) use ($request) {

                        $value->start_btn_status = $value->end_btn_status = $value->cancel_btn_status =$value->join_btn_status =  NO;

                        if($request->id == $value->user_id) {

                        	$value->start_btn_status = $value->status == MEETING_SCHEDULED ? YES : NO;

                        	$value->end_btn_status = $value->status == MEETING_STARTED ? YES : NO;

                        	$value->cancel_btn_status = in_array($value->status, [MEETING_SCHEDULED]) ? YES : NO;

                        } else {

                        	$value->join_btn_status = $value->status == MEETING_STARTED ? YES : NO;

                        }

                        $value->schedule_time = common_date($value->schedule_time, $request->timezone, 'd M Y h:i A');

                        return $value;
                    });


        return $data;

    }

    public static function last_x_days_meetings($request, $days = 10) {
            
        $start  = new \DateTime('-7 day', new \DateTimeZone('UTC'));
        
        $period = new \DatePeriod($start, new \DateInterval('P1D'), $days);
       
        $dates = $last_x_days_meetings = [];

        foreach ($period as $date) {

            $current_date = $date->format('Y-m-d');

            $last_x_days_data = new \stdClass;

            $last_x_days_data->date = date('d-m-y', strtotime($current_date));
          
            $total_count = \App\Meeting::where('user_id', $request->id)->whereDate('created_at', '=', $current_date)->count();
          
            $last_x_days_data->total = $total_count ?: 0;

            array_push($last_x_days_meetings, $last_x_days_data);

        }
                
        return $last_x_days_meetings;   

    }


}