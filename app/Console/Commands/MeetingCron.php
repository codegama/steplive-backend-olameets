<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Meeting;
use Carbon\Carbon;

class MeetingCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'meeting:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Used to update old meetings which is not ended properly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $schedule_time = date('Y-m-d H:00:00',strtotime("-1 hour"));

        $end_status = [MEETING_NONE, MEETING_SCHEDULED];

        Meeting::where('schedule_time', '<=', $schedule_time)->whereIn('status', $end_status)->chunk(100, function ($meetings) {
            
            foreach ($meetings as $meeting) {

                $meeting->status = MEETING_ENDED;

                $meeting->start_time = $meeting->start_time ?: date('Y-m-d H:i:s');

                $meeting->end_time = date('Y-m-d H:i:s');

                $meeting->save();
            
            }
        });
    }
}
