<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingRecord extends Model
{
    public function meetingDetails() {

    	return $this->belongsTo(Meeting::class,'meeting_id');
    }
}
