<!-- Vendor js -->
<script src="{{asset('admin-assets/assets/js/vendor.min.js')}}"></script>

<!-- App js -->
<script src="{{asset('admin-assets/assets/js/app.min.js')}}"></script>

 <!-- datatable js -->
<script src="{{asset('admin-assets/assets/libs/datatables/jquery.dataTables.min.js')}}"></script>

<script src="{{asset('admin-assets/assets/libs/datatables/dataTables.bootstrap4.min.js')}}"></script>

<script src="{{asset('admin-assets/assets/libs/datatables/dataTables.responsive.min.js')}}"></script>

<script src="{{asset('admin-assets/assets/libs/datatables/responsive.bootstrap4.min.js')}}"></script>

<script src="{{asset('admin-assets/assets/libs/datatables/dataTables.buttons.min.js')}}"></script>

<script src="{{asset('admin-assets/assets/libs/datatables/buttons.bootstrap4.min.js')}}"></script>

<script src="{{asset('admin-assets/assets/libs/datatables/buttons.html5.min.js')}}"></script>

<script src="{{asset('admin-assets/assets/libs/datatables/buttons.flash.min.js')}}"></script>

<script src="{{asset('admin-assets/assets/libs/datatables/buttons.print.min.js')}}"></script>

<script src="{{asset('admin-assets/assets/libs/datatables/dataTables.keyTable.min.js')}}"></script>

<script src="{{asset('admin-assets/assets/libs/datatables/dataTables.select.min.js')}}"></script>

<!-- Datatables init -->
<script src="{{asset('admin-assets/assets/js/pages/datatables.init.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<!-- daterangepicker -->

<script src="{{asset('admin-assets/assets/daterangepicker/moment.min.js')}}"></script>

<script src="{{asset('admin-assets/assets/daterangepicker/daterangepicker.js')}}"></script>s


<script type="text/javascript">

    $(document).ready(function(){
   
        setTimeout(function(){

        $('#basic-datatable_filter').hide();

            $('#basic-datatable_filter').hide();
            
         },100),

        setTimeout(function(){

        $('#basic-datatable_length').hide();

            $('#basic-datatable_length').hide();
            
         },100),

        setTimeout(function(){
            
        $('#basic-datatable_paginate').hide();

            $('#basic-datatable_paginate').hide();
            
         },100);

     });

</script>

<script type="text/javascript">

    $("#{{$main_page}}").addClass("mm-active");

    @if(isset($sub_page))

        $("#{{$page}}").addClass('mm-show'); 

        var meeting = "{{$sub_page}}";
        
        if(meeting == 'meetings-scheduled') {
            $("#meetings-index").removeClass("active"); 
        }

        $("#{{$sub_page}}").addClass("active"); 

    @endif

    $(document).ready(function() {

        $('.select2').select2();

        $('select').on("select2:close", function () { $(this).focus(); });

        $('.js-example-basic-multiple').select2();

    });

</script>

