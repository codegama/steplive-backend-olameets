@extends('layouts.admin') 

@section('content-header', tr('users'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.users.index')}}">{{tr('users')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('view_users') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                <div class="border-bottom pb-3">

                    <h5 class="text-uppercase">{{tr('view_users')}}

                        <a class="btn btn-outline-primary float-right user_add" href="{{route('admin.users.create')}}">
                            <i class="fa fa-plus"></i> {{tr('add_user')}}
                        </a>
                  
                    </h5>

                </div>

                @include('admin.users._search')
                
                <table id="basic-datatable" class="table dt-responsive nowrap">

                    <thead>
                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('name')}}</th>
                            <th>{{tr('email')}}</th>
                            <th>{{tr('mobile')}}</th>   
                            <th>{{tr('status')}}</th>
                            <th>{{tr('verify')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($users as $i => $user_details)
                              
                            <tr>
                                <td>{{$i+$users->firstItem()}}</td>

                                <td>
                                    <a href="{{route('admin.users.view' , ['user_id' => $user_details->id])}}"> {{ $user_details->name }}
                                    </a>
                                </td>

                                <td> {{ $user_details->email }} </td>

                                <td> {{ ($user_details->mobile > 1) ? $user_details->mobile : tr('unavailable')}} </td>

                                <td>

                                    @if($user_details->status == USER_APPROVED)

                                        <span class="badge badge-success">{{ tr('approved') }} </span>

                                    @else

                                        <span class="badge badge-danger">{{ tr('declined') }} </span>

                                    @endif

                                </td>

                                <td>   

                                    @if($user_details->is_verified == USER_EMAIL_VERIFIED) 

                                        <span class="badge badge-success">{{ tr('verified') }} </span>

                                    @else

                                        <a class="badge badge-info" href="{{ route('admin.users.verify', ['user_id' => $user_details->id]) }}"> 
                                            {{ tr('verify') }} 
                                        </a>

                                    @endif  
                                                                  
                                </td>

                                <td>     

                                    <div class="template-demo">

                                        <div class="dropdown">

                                            <button class="btn btn-outline-primary  dropdown-toggle" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{tr('action')}}
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                                              
                                                <a class="dropdown-item" href="{{ route('admin.users.view', ['user_id' => $user_details->id]) }}">
                                                    {{tr('view')}}
                                                </a>
                                                
                                                @if(Setting::get('is_demo_control_enabled') == NO)
                                                    <a class="dropdown-item" href="{{ route('admin.users.edit', ['user_id' => $user_details->id]) }}">
                                                        {{tr('edit')}}
                                                    </a>
                                                    
                                                    <a class="dropdown-item" href="{{route('admin.users.delete', ['user_id' => $user_details->id])}}" 
                                                    onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $user_details->name)}}&quot;);">
                                                        {{tr('delete')}}
                                                    </a>
                                                @else

                                                    <a class="dropdown-item text-muted" href="javascript:;">{{tr('edit')}}</a>
                                                  
                                                    <a class="dropdown-item text-muted" href="javascript:;">{{tr('delete')}}</a>                           
                                                @endif

                                                <div class="dropdown-divider"></div>

                                                @if($user_details->is_verified == USER_EMAIL_NOT_VERIFIED) 

                                                    <a class="dropdown-item" href="{{ route('admin.users.verify', ['user_id' => $user_details->id]) }}"> {{ tr('verify') }} 
                                                    </a>

                                                @endif 

                                                @if($user_details->status == USER_APPROVED)

                                                    <a class="dropdown-item" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}" onclick="return confirm(&quot;{{$user_details->first_name}} - {{tr('user_decline_confirmation')}}&quot;);" >
                                                        {{ tr('decline') }} 
                                                    </a>

                                                @else
                                                    
                                                    <a class="dropdown-item" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}">
                                                        {{ tr('approve') }} 
                                                    </a>
                                                       
                                                @endif

                                                 <a class="dropdown-item" href="{{route('admin.users.subscriptions_index',['user_id' => $user_details->id])}}">{{tr('subscriptions')}}</a>

                                            </div>

                                        </div>

                                    </div>

                                </td>

                            </tr>

                        @endforeach
                    </tbody>
                   
                </table>

                <div class="float-right" id="paglink">{{ $users->appends(request()->input())->links() }}</div>

            </div>
            
        </div>
       
    </div>
   
</div>




@endsection