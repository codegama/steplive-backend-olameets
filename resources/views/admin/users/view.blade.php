@extends('layouts.admin') 

@section('content-header', tr('users'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.users.index')}}">{{tr('users')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('view_users') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-body">

				<div class="row">

				   <div class="col">

				      <div class="card">

				         <div class="card-body p-0">

				            <h4 class="card-title border-bottom p-3 mb-0 header-title text-uppercase">{{tr('view_users')}}</h4>

				            <div class="row py-1">

				               <div class="col-xl-3 col-sm-6">

				                  	<div class="media p-3">

				                     	<i data-feather="grid" class="align-self-center icon-dual icon-lg mr-4"></i>

					                    <div class="media-body">
					                        <h4 class="mt-0 mb-0">{{$user_details->total_meetings}}</h4>
					                        <span class="text-muted font-size-13">{{tr('total_meetings')}}</span>
					                    </div>

				                  	</div>

				               	</div>

				               	<div class="col-xl-3 col-sm-6">

 				                  	<div class="media p-3">

				                     	<i data-feather="check-square" class="align-self-center icon-dual icon-lg mr-4"></i>

					                    <div class="media-body">
					                        <h4 class="mt-0 mb-0">{{$user_details->today_meetings}}</h4>
					                        <span class="text-muted">{{tr('today_meetings')}}</span>
					                    </div>

				                  	</div>

				               </div>

				               <div class="col-xl-3 col-sm-6">

				                 	<div class="media p-3">

				                     	<i data-feather="clock"
				                        class="align-self-center icon-dual icon-lg mr-4"></i>

				                    	<div class="media-body">
				                        	<h4 class="mt-0 mb-0">{{$user_details->upcoming_meetings}}</h4>
				                        	<span class="text-muted">{{tr('upcoming_meetings')}}</span>
				                     	</div>

				                  	</div>

				               </div>

				               <div class="col-xl-3 col-sm-6">

				                  	<div class="media p-3">

				                    <i data-feather="trash"
				                        class="align-self-center icon-dual icon-lg mr-4"></i>
				                     	<div class="media-body">
					                        <h4 class="mt-0 mb-0">{{$user_details->cancelled_meetings}}</h4>
					                        <span class="text-muted">{{tr('cancelled_meetings')}}</span>
				                     	</div>

				                  	</div>

				               </div>

				            </div>

				         </div>

				      </div>

				   </div>

				</div>

				<div class="row">
					
					<div class="col-md-6">

					    <div class="card card-widget widget-user-2">

					        <div class="widget-user-header pb-3">
					            <div class="widget-user-image">
					                <img class="user-image" src="{{$user_details->picture ?: asset('placeholder.jpeg')}}" alt="User Avatar" />
					            </div>
					        </div>

					        <h6 class="card-title border-bottom p-3 mb-0 header-title">{{tr('action')}}</h6>

					        @if(Setting::get('is_demo_control_enabled') == NO)

					        	<div class="row pt-3">

					                <div class="col-md-3">
					            		<a class="btn btn-success width-md"  href="{{ route('admin.users.edit', ['user_id' => $user_details->id]) }}">
				                    {{tr('edit')}}
				                	</a>
					            	</div>

					                <div class="col-md-3">
					                    <a class="btn btn-danger width-md" href="{{route('admin.users.delete', ['user_id' => $user_details->id])}}" 
					                    onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $user_details->name)}}&quot;);">
					                        {{tr('delete')}}
					                    </a>
					                </div>

					            </div>

					        @else
					    		<div class="row">

					            	<div class="col-md-3">
					            		<a class="btn btn-success width-md" href="user_details:;">{{tr('edit')}}</a>
					            	</div>

					            	<div class="col-md-3">
					                	<a class="btn btn-delete width-md" href="javascript:;" onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $user_details->name)}}&quot;);">{{tr('delete')}}</a>
					                </div>

					            </div>
					                                      
					        @endif

					        <div class="row pt-3">

					        	<div class="col-md-3">

					        		@if($user_details->status == APPROVED)

					                    <a class="btn btn-danger width-md" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}" onclick="return confirm(&quot;{{$user_details->name}} - {{tr('user_decline_confirmation')}}&quot;);" >
					                        {{ tr('decline') }} 
					                    </a>

					                @else
					                    
					                    <a class="btn btn-success width-md" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}">
					                        {{ tr('approve') }} 
					                    </a>
					                       
					                @endif
					        	</div>

					        	<div class="col-md-3">
					        		 @if($user_details->is_verified == USER_EMAIL_NOT_VERIFIED) 

					                    <a class="btn btn-info width-md" href="{{ route('admin.users.verify', ['user_id' => $user_details->id]) }}"> {{ tr('verify') }} 
					                    </a>

					                @endif 
					            </div>

					        </div>

					        <br>

					    </div>

					</div>

					<div class="col-md-6">
					    
					    <div class="card card-widget widget-user-2">

					        <div class="card-body p-0">

					            <ul class="nav flex-column">

					            	<li class="nav-item">
					                    <div href="#" class="nav-link">{{tr('name')}} <span class="float-right text-uppercase">{{$user_details->name}}</span> </div>
					                </li>

					            	<li class="nav-item">
					                    <div href="#" class="nav-link">{{tr('email')}}<span class="float-right text-uppercase">{{$user_details->email}}</span> </div>
					                </li>

					                <li class="nav-item">
					                   <div class="nav-link">{{tr('mobile')}}<span class="float-right text-uppercase">{{$user_details->mobile ?: tr('unavailable')}}</span> </div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link"> {{tr('payment_mode')}} <span class="float-right badge badge-secondary text-uppercase">{{$user_details->payment_mode}}</span></div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('is_verified')}} 
					                    	@if($user_details->is_verified == USER_EMAIL_VERIFIED)
					                    		<span class="float-right badge badge-primary text-uppercase">{{tr('yes')}}</span>
					                    	@else
					                    	 	<span class="float-right badge badge-danger text-uppercase">{{tr('no')}}</span>
					                    	@endif
					                    </div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('status')}}
					                    	@if($user_details->status == APPROVED)
					                    		<span class="float-right badge badge-success text-uppercase">{{tr('approved')}}</span>
					                    	@else
					                    	 	<span class="float-right badge badge-danger text-uppercase">{{tr('declined')}}</span>
					                    	@endif 
					                    </div>
					                </li>

					                <li class="nav-item">
					                    <div href="#" class="nav-link">{{tr('device_type')}} <span class="float-right badge badge-info text-uppercase">{{$user_details->device_type}}</span> </div>
					                </li>

					                <li class="nav-item">
					                   <div class="nav-link">{{tr('login_by')}}<span class="float-right text-uppercase">{{$user_details->login_by}}</span></div>
					                </li>


					                <li class="nav-item">
					                    <div class="nav-link">{{tr('created_at')}}<span class="float-right">{{common_date($user_details->created_at,Auth::guard('admin')->user()->timezone)}}</span></div>
					                </li>

					                <li class="nav-item">
					                    <div class="nav-link">{{tr('updated_at')}}<span class="float-right">{{common_date($user_details->updated_at,Auth::guard('admin')->user()->timezone)}}</span></div>
					                </li>

					            </ul>

					        </div>

					    </div>
					   
					</div>

				</div>

            </div>
            
        </div>
        
    </div>
    
</div>

@endsection

