<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                <div class="border-bottom pb-3">

                    <h5 class="text-uppercase">{{$user_details->id ? tr('edit_user') : tr('add_user')}}

                        <a class="btn btn-outline-primary float-right" href="{{route('admin.users.index')}}">
                            <i class="fa fa-plus"></i> {{tr('view_users')}}
                        </a>
                  
                    </h5>

                </div>

                <form class="forms-sample pt-3" action="{{ Setting::get('is_demo_control_enabled') == NO ? route('admin.users.save') : '#'}}" method="POST" enctype="multipart/form-data" role="form">
                    @csrf

                   
                    @if($user_details->id)

                    <input type="hidden" name="user_id" id="user_id" value="{{$user_details->id}}" />

                    @endif

                    <input type="hidden" name="login_by" id="login_by" value="{{$user_details->login_by ?: 'manual'}}" />

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="name">{{ tr('name') }} <span class="admin-required">*</span> </label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="{{ tr('name') }}" value="{{ old('name') ?: $user_details->name}}" required />
                        </div>

                        <div class="form-group col-md-6">
                            <label for="mobile">{{ tr('mobile') }} </label>

                            <input type="number" minlength="10" maxlength="12" class="form-control" pattern="[0-9]{6,13}" id="mobile" name="mobile" placeholder="{{ tr('mobile') }}" value="{{ old('mobile') ?: $user_details->mobile}}"/>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="email">{{ tr('email')}} <span class="admin-required">*</span></label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="{{ tr('email')}}" value="{{ old('email') ?: $user_details->email}}" required />
                        </div>

                        @if(!$user_details->id)

                        <div class="form-group col-md-6">
                            <label for="password">{{ tr('password') }} <span class="admin-required">*</span></label>
                            <input type="password" class="form-control" id="password" name="password" placeholder="{{ tr('password') }}" value="{{old('password')}}" required title="{{ tr('password_notes') }}" />
                        </div>

                        @endif
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>{{tr('upload_image')}}</label>

                            <div class="input-group col-xs-12">
                                <input type="file" class="form-control file-upload-info" name="picture" placeholder="{{tr('upload_image')}}" accept="image/*" />

                                <div class="input-group-append">
                                    <button class="btn btn-info" type="button">{{tr('upload')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <button type="reset" class="btn btn-warning">{{ tr('reset')}}</button>

                    @if(Setting::get('is_demo_control_enabled') == NO )

                    <button type="submit" class="btn btn-success mr-2">{{ tr('submit') }}</button>

                    @else

                    <button type="button" class="btn btn-success mr-2" disabled>{{ tr('submit') }}</button>

                    @endif
                   
                </form>

            </div>

        </div>

    </div>

</div>
