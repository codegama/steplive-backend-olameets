@extends('layouts.admin')

@section('content-header', tr('meetings'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.meetings.index') }}">{{tr('meetings')}}</a></li>
    
    <li class="breadcrumb-item active" aria-current="page">
    	<span>{{ tr('edit_meeting') }}</span>
    </li>
           
@endsection 

@section('content')

	@include('admin.meetings._form')

@endsection

@section('scripts')

<script>
    jQuery(document).ready(function() {
        jQuery('input[name="start_time"]').daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD MMM Y h:mm A'
            }
        });
        jQuery('input[name="start_time"]').on('apply.daterangepicker', function(ev, picker) {
            jQuery(this).val(picker.startDate.format('DD MMM Y h:mm A'));
        });
        jQuery('input[name="start_time"]').on('cancel.daterangepicker', function(ev, picker) {
            jQuery(this).val('');
        });
    });

    jQuery(document).ready(function() {
        jQuery('input[name="schedule_time"]').daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD MMM Y h:mm A'
            }
        });
        jQuery('input[name="schedule_time"]').on('apply.daterangepicker', function(ev, picker) {
            jQuery(this).val(picker.startDate.format('DD MMM Y h:mm A'));
        });
        jQuery('input[name="schedule_time"]').on('cancel.daterangepicker', function(ev, picker) {
            jQuery(this).val('');
        });
    });

    jQuery(document).ready(function() {
        jQuery('input[name="end_time"]').daterangepicker({
            autoUpdateInput: false,
            singleDatePicker: true,
            timePicker: true,
            startDate: moment().startOf('hour'),
            endDate: moment().startOf('hour').add(32, 'hour'),
            locale: {
                cancelLabel: 'Clear',
                format: 'DD MMM Y h:mm A'
            }
        });
        jQuery('input[name="end_time"]').on('apply.daterangepicker', function(ev, picker) {
            jQuery(this).val(picker.startDate.format('DD MMM Y h:mm A'));
        });
        jQuery('input[name="end_time"]').on('cancel.daterangepicker', function(ev, picker) {
            jQuery(this).val('');
        });
    });

</script>
@endsection