@extends('layouts.admin') 

@section('content-header', tr('meetings'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.meetings.index')}}">{{tr('meetings')}}</a></li>

    @if(Request::get('type'))

        <li class="breadcrumb-item active" aria-current="page">
            <span>{{ tr('scheduled_meetings') }}</span>
        </li> 

    @else
        <li class="breadcrumb-item active" aria-current="page">
            <span>{{ tr('view_meetings') }}</span>
        </li> 
    @endif

@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                <div class="border-bottom pb-3">

                    <h5 class="text-uppercase">

                        @if(Request::get('type'))

                            {{tr('scheduled_meetings')}}

                        @else

                            {{tr('view_meetings')}}

                        @endif
                        <!-- <a class="btn btn-outline-primary float-right" href="{{route('admin.meetings.create')}}">
                            <i class="fa fa-plus"></i> {{tr('add_meeting')}}
                        </a> -->
                  
                    </h5>

                </div>
                
                @include('admin.meetings._search')

                <table  id="basic-datatable" class="table dt-responsive nowrap">

                    <thead>

                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('unique_id')}}</th>
                            <th>{{tr('title')}}</th>
                            <th>{{tr('user')}}</th>
                            <th>{{tr('schedule_time')}}</th>
                            <th>{{tr('total_users')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>

                    </thead>

                    <tbody>

                        @foreach($meetings as $i => $meeting_details)
                             
                            <tr>
                                <td>{{$i+$meetings->firstItem()}}</td>

                                <td>
                                    <a href="{{route('admin.meetings.view',['meeting_id' => $meeting_details->id])}}"> {{ $meeting_details->unique_id }}
                                    </a>
                                </td>
                               
                                <td> 
                                    {{ $meeting_details->title }} 
                                </td>

                                <td>
                                    <a href="{{route('admin.users.view',['user_id' => $meeting_details->user_id])}}"> {{ $meeting_details->userDetails->name ?? "-" }}
                                    </a>
                                </td>

                                <td>
                                    {{common_date($meeting_details->schedule_time,Auth::guard('admin')->user()->timezone)}}

                                </td>
                               
                                <td>
                                    {{ $meeting_details->users_count }}
                                </td>

                                <td>

                                   <span class="badge badge-secondary"> {{$meeting_details->status_formatted}}</span>

                                </td>

                                <td>     

                                    <div class="template-demo">

                                        <div class="dropdown">

                                            <button class="btn btn-outline-primary  dropdown-toggle" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{tr('action')}}
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                                              
                                                <a class="dropdown-item" href="{{ route('admin.meetings.view', ['meeting_id' => $meeting_details->id]) }}">
                                                    {{tr('view')}}
                                                </a>
                                                
                                                @if(Setting::get('is_demo_control_enabled') == NO)
                                                    <a class="dropdown-item" href="{{ route('admin.meetings.edit', ['meeting_id' => $meeting_details->id]) }}">
                                                        {{tr('edit')}}
                                                    </a>
                                                    
                                                    <a class="dropdown-item" href="{{route('admin.meetings.delete', ['meeting_id' => $meeting_details->id])}}" 
                                                    onclick="return confirm(&quot;{{tr('meeting_delete_confirmation' , $meeting_details->title)}}&quot;);">
                                                        {{tr('delete')}}
                                                    </a>
                                                @else

                                                    <a class="dropdown-item text-muted" href="javascript:;">{{tr('edit')}}</a>
                                                  
                                                    <a class="dropdown-item text-muted" href="javascript:;" onclick="return confirm(&quot;{{tr('meeting_delete_confirmation' , $meeting_details->title)}}&quot;);">{{tr('delete')}}</a>                           
                                                @endif

                                                <div class="dropdown-divider"></div>

<!-- 
                                                @if($meeting_details->status == APPROVED)

                                                    <a class="dropdown-item" href="{{ route('admin.meetings.status', ['meeting_id' => $meeting_details->id]) }}" onclick="return confirm(&quot;{{$meeting_details->title}} - {{tr('meeting_decline_confirmation')}}&quot;);" >
                                                        {{ tr('decline') }} 
                                                    </a>

                                                @else
                                                    
                                                    <a class="dropdown-item" href="{{ route('admin.meetings.status', ['meeting_id' => $meeting_details->id]) }}">
                                                        {{ tr('approve') }} 
                                                    </a>
                                                       
                                                @endif -->


                                            </div>

                                        </div>

                                    </div>

                                </td>

                            </tr>

                        @endforeach
                        
                    </tbody>
                   
                </table>

                <div class="float-right">{{ $meetings->appends(request()->input())->links() }}</div>
                
            </div>
            
        </div>
       
    </div>
   
</div>


@endsection