<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                <h5 class="border-bottom pb-4 text-uppercase">

                    {{$meeting_details->id ? tr('edit_meeting') : tr('add_meeting')}}

                    <a class="btn btn-outline-primary float-right" href="{{route('admin.meetings.index')}}"> <i class="fa fa-eye"></i> {{tr('view_meetings')}} </a>

                </h5>

                <form class="forms-sample pt-3" action="{{ Setting::get('is_demo_control_enabled') == NO ? route('admin.meetings.save') : '#'}}" method="POST" enctype="multipart/form-data" role="form">
                    @csrf

                        @if($meeting_details->id)

                        <input type="hidden" name="meeting_id" id="meeting_id" value="{{$meeting_details->id}}" />

                        @endif

                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="title">{{ tr('title') }} <span class="admin-required">*</span> </label>
                                <input type="text" class="form-control" id="title" name="title" placeholder="{{ tr('title') }}" value="{{ old('title') ?: $meeting_details->title}}" required />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="schedule_time">{{ tr('schedule_time') }}</label>
                                <input type="text" class="form-control" id="schedule_time" name="schedule_time" placeholder="{{ tr('schedule_time') }}" value="{{ old('schedule_time') ?: ($meeting_details->schedule_time ?: common_date($meeting_details->schedule_time,Auth::guard('admin')->user()->timezone))}}" />
                            </div>

                        </div>

                        <div class="row">

                            <div class="form-group col-md-6">
                                <label for="start_time">{{ tr('start_time') }} <span class="admin-required">*</span> </label>
                                <input type="text" class="form-control" id="start_time" name="start_time" placeholder="{{ tr('start_time') }}" value="{{ old('start_time') ?: ($meeting_details->start_time ?: common_date($meeting_details->start_time,Auth::guard('admin')->user()->timezone))}}" required />
                            </div>

                            <div class="form-group col-md-6">
                                <label for="end_time">{{ tr('end_time') }} <span class="admin-required">*</span> </label>
                                <input type="text" class="form-control" id="end_time" name="end_time" placeholder="{{ tr('end_time') }}" value="{{ old('end_time') ?: ($meeting_details->end_time ?: common_date($meeting_details->end_time,Auth::guard('admin')->user()->timezone))}}" required />
                            </div>
                        </div>

                        <div class="row">

                             <div class="form-group col-md-6">

                                <label for="title">{{ tr('select_users') }} <span class="admin-required">*</span> </label>

                                <select class="form-control select2" id="user_id" name="user_id">
                             
                                    @foreach($users as $user_details)
                                        <option value="{{$user_details->id}}"@if($user_details->is_selected == YES) selected @endif>
                                            {{$user_details->name}}
                                        </option>
                                    @endforeach

                                </select>

                            </div>

                            <div class="form-group col-md-6">

                                <label for="title">{{ tr('select_users') }} <span class="admin-required">*</span> </label>

                                <select class="form-control js-example-basic-multiple select2" id="user_id" name="user_ids[]" multiple="multiple">
                             
                                    @foreach($users as $user_details)
                                        <option value="{{$user_details->id}}"@if($user_details->is_selected == YES) selected @endif>
                                            {{$user_details->name}}
                                        </option>
                                    @endforeach

                                </select>

                            </div>

                            <div class="form-group col-md-6">
                                <label>{{tr('upload_image')}}</label>

                                <div class="input-group col-xs-12">
                                    <input type="file" class="form-control file-upload-info" name="picture" placeholder="{{tr('upload_image')}}" accept="image/*" />

                                    <div class="input-group-append">
                                        <button class="btn btn-info" type="button">{{tr('upload')}}</button>
                                    </div>
                                </div>
                            </div>    
                        </div>

                        <div class="row">

                            <div class="form-group col-md-12">

                                <label for="simpleMde">{{ tr('description') }}</label>

                                <textarea class="form-control" id="description" name="description">{{ old('description') ?: $meeting_details->description}}</textarea>

                            </div>

                        </div>

                    <button type="reset" class="btn btn-warning">{{ tr('reset')}}</button>

                    @if(Setting::get('is_demo_control_enabled') == NO )

                    <button type="submit" class="btn btn-success mr-2">{{ tr('submit') }}</button>

                    @else

                    <button type="button" class="btn btn-success mr-2" disabled>{{ tr('submit') }}</button>

                    @endif

                </form>
            
            </div>

        </div>

    </div>

</div>
