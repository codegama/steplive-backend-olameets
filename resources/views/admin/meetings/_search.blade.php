<form action="{{route('admin.meetings.index')}}" method="GET" role="search">

<div class="row pt-2 pb-2">

    <div class="col-3">
        @if(Request::has('search_key'))
        <p class="text-muted">{{tr('search_results_for')}}<b>{{Request::get('search_key')}}</b></p>
        @endif
    </div>

    <div class="col-3">

        <select class="form-control select2" name="meeting_type">

            <option class="select-color" value="">{{tr('select_meeting_type')}}</option>

            <option class="select-color" value="{{SORT_BY_MEETING_UPCOMING}}">{{tr('upcoming')}}</option>

            <option class="select-color" value="{{SORT_BY_MEETING_ONLIVE}}">{{tr('onlive')}}</option>

            @if(!Request::get('type'))

            <option class="select-color" value="{{SORT_BY_MEETING_CANCELLED}}">{{tr('cancelled')}}</option>

            <option class="select-color" value="{{SORT_BY_MEETING_ENDED}}">{{tr('ended')}}</option>

            @endif
        </select>

    </div>


    <div class="col-2">

        <select class="form-control select2" name="status">

            <option class="select-color" value="">{{tr('select_status')}}</option>

            <option class="select-color" value="{{MEETING_NONE}}" @if(Request::get('status') == MEETING_NONE && Request::get('status')!='') selected @endif>{{tr('none')}}</option>

            <option class="select-color" value="{{MEETING_SCHEDULED}}" @if(Request::get('status') == MEETING_SCHEDULED && Request::get('status')!='') selected @endif>{{tr('MEETING_SCHEDULED')}}</option>

            <option class="select-color" value="{{MEETING_STARTED}}" @if(Request::get('status') == MEETING_STARTED && Request::get('status')!='') selected @endif>{{tr('MEETING_STARTED')}}</option>

            <option class="select-color" value="{{MEETING_ENDED}}" @if(Request::get('status') == MEETING_ENDED && Request::get('status')!='') selected @endif>{{tr('MEETING_ENDED')}}</option>

            <option class="select-color" value="{{MEETING_CANCELLED}}" @if(Request::get('status') == MEETING_CANCELLED && Request::get('status')!='') selected @endif>{{tr('MEETING_CANCELLED')}}</option>

        </select>

    </div>


    <div class="col-4">

        <div class="input-group">
            <input type="text" class="form-control" name="search_key" placeholder="{{tr('meetings_search_placeholder')}}"> <span class="input-group-btn">

                &nbsp

                <button type="submit" class="btn btn-primary btn-width">
                    {{tr('search')}}
                </button>

                <a class="btn btn-primary" href="{{route('admin.meetings.index')}}">{{tr('clear')}}
                </a>
                </span>
            </div>

        </div>

    </div>

</form>