@extends('layouts.admin')

@section('content-header')

  {{tr('dashboard')}}

@endsection

@section('bread-crumb')

<li class="breadcrumb-item active" aria-current="page">
    <span>{{ tr('dashboard') }}</span>
</li>

@endsection

@section('content')

<div class="row">
    <div class="col-md-6 col-xl-3">
        <div class="card">
            <div class="card-body p-0">
                <div class="media p-3">
                    <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{tr('total_users')}}</span>
                        <h2 class="mb-0">{{$data->total_users}}</h2>
                    </div>
                    <div class="align-self-center">
                        <span class="icon-lg icon-dual-primary" data-feather="users"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card">
            <div class="card-body p-0">
                <div class="media p-3">
                    <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{tr('today_users')}}</span>
                        <h2 class="mb-0">{{$data->today_users}}</h2>
                    </div>
                    <div class="align-self-center">
                        <span class="icon-lg icon-dual-success" data-feather="user"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card">
            <div class="card-body p-0">
                <div class="media p-3">
                    <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{tr('total_revenue')}}</span>
                        <h2 class="mb-0">{{formatted_amount($data->total_revenue)}}</h2>
                    </div>
                    <div class="align-self-center">
                        <span class="icon-lg icon-dual-info" data-feather="bar-chart"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-6 col-xl-3">
        <div class="card">
            <div class="card-body p-0">
                <div class="media p-3">
                    <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{tr('today_revenue')}}</span>
                        <h2 class="mb-0">{{formatted_amount($data->today_revenue)}}</h2>
                    </div>
                    <div class="align-self-center">
                        <span class="icon-lg icon-dual-warning" data-feather="inbox"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-xl-6">
        <!-- Portlet card -->
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mt-0 mb-3">{{tr('recently_visited_users')}}</h4>

                <div id="recently_visited_users_chart" class="apex-charts" dir="ltr"></div>
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-xl-6">
        <!-- Portlet card -->
        <div class="card">
            <div class="card-body">
                <h4 class="header-title mt-0 mb-3">{{tr('overall_subscription')}}</h4>

                <div id="overall_subscription_chart" class="apex-charts" dir="ltr"></div>
            </div> <!-- end card-body -->
        </div> <!-- end card-->
    </div> <!-- end col-->

</div>

<!-- Meetings and users details -->

<div class="row">
    
    <div class="col-xl-4">
        <div class="card">
            <div class="card-body p-0">
                <h5 class="card-title header-title border-bottom p-3 mb-0">Meetings Overview</h5>
                <!-- stat 1 -->
                <div class="media px-3 py-4 border-bottom">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{$data->total_meetings}}</h4>
                        <span class="text-muted">{{tr('total_meetings')}}</span>
                    </div>
                    <i data-feather="users" class="align-self-center icon-dual icon-lg"></i>
                </div>

                <!-- stat 2 -->
                <div class="media px-3 py-4 border-bottom">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{$data->scheduled_meetings}}</h4>
                        <span class="text-muted">{{tr('scheduled_meetings')}}</span>
                    </div>
                    <i data-feather="image" class="align-self-center icon-dual icon-lg"></i>
                </div>

                <!-- stat 3 -->
                <div class="media px-3 py-4">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{$data->today_meetings}}</h4>
                        <span class="text-muted">{{tr('todays_meetings')}}</span>
                    </div>
                    <i data-feather="shopping-bag" class="align-self-center icon-dual icon-lg"></i>
                </div>

                <!-- stat 3 -->
                <div class="media px-3 py-4">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{$data->completed_meetings}}</h4>
                        <span class="text-muted">{{tr('completed_meetings')}}</span>
                    </div>
                    <i data-feather="shopping-bag" class="align-self-center icon-dual icon-lg"></i>
                </div>

                <div class="media px-3 py-4">
                    <div class="media-body">
                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{$data->cancelled_meetings}}</h4>
                        <span class="text-muted">{{tr('cancelled_meetings')}}</span>
                    </div>
                    <i data-feather="shopping-bag" class="align-self-center icon-dual icon-lg"></i>
                </div>
            </div>
        </div>
    
    </div>

    <div class="col-xl-4">
        <div class="card">
            <div class="card-body pt-2">
                <h5 class="mb-4 header-title">{{tr('currently_on_live')}}</h5>

                @if($data->on_live_meetings->isNotEmpty())

                    @foreach($data->on_live_meetings as $on_live_meeting)

	                	<a href="{{route('admin.meetings.view',['meeting_id' => $on_live_meeting->id])}}">

			                <div class="media border-top pt-3">
			                    <img src="{{$on_live_meeting->userDetails->picture ?? '-'}}" class="avatar rounded mr-3"
			                        alt="meetings">
			                    <div class="media-body">
			                        <h6 class="mt-1 mb-0">{{$on_live_meeting->title}}</h6>
			                        <p class="text-muted font-size-12 mt-1 mb-3">
                                        {{common_date($on_live_meeting->schedule_time,Auth::guard('admin')->user()->timezone)}}
                                       </p>
			                    </div>
			                
		                	</div>

	                	</a>

                	@endforeach

                	<a href="{{route('admin.meetings.index')}}" type="button" class="btn btn-block btn--md btn-primary">{{tr('view_all')}}</a>

                @else

                    <div class="text-center m-5">
                        <h2 class="text-muted"><i class="fa fa-inbox"></i></h2>
                        <p>{{tr('on_live_no_results')}}</p> 
                    </div>

                @endif
                
            </div>
        </div>
    </div>

    <div class="col-xl-4">
        <div class="card">
            <div class="card-body pt-2">
                <h5 class="mb-4 header-title">{{tr('today_scheduled_meetings')}}</h5>

                @if($data->today_scheduled_meetings->isNotEmpty())

                    @foreach($data->today_scheduled_meetings as $meeting_details)

	                	<a href="{{route('admin.meetings.view',['meeting_id' => $meeting_details->id])}}">

			                <div class="media border-top pt-3">
			                    <img src="{{$meeting_details->userDetails->picture ?? '-'}}" class="avatar rounded mr-3"
			                        alt="meetings">
			                    <div class="media-body">

			                    	<h6 class="mt-1 mb-0">{{$meeting_details->title}}</h6>
			                        <p class="text-muted font-size-12 mt-1 mb-3"> {{common_date($meeting_details->schedule_time,Auth::guard('admin')->user()->timezone)}}</p>
			                    </div>
			                
		                	</div>

	                	</a>

                	@endforeach

                	<a href="{{route('admin.meetings.index',['type' => 'scheduled'])}}" type="button" class="btn btn-block btn--md btn-primary">{{tr('view_all')}}</a>

                @else

                    <div class="text-center m-5">
                        <h2 class="text-muted"><i class="fa fa-inbox"></i></h2>
                        <p>{{tr('today_scheduled_meetings_no_results')}}</p> 
                    </div>

                @endif
                
            </div>
        </div>
    </div>
   
</div>
<!-- row -->

@endsection

@section('scripts')

<script src="https://apexcharts.com/samples/assets/irregular-data-series.js"></script>

<script src="https://apexcharts.com/samples/assets/series1000.js"></script>

<script src="https://apexcharts.com/samples/assets/ohlc.js"></script>

<!-- third party:js -->
<script src="{{asset('admin-assets/assets/libs/apexcharts/apexcharts.min.js')}}"></script>
<!-- third party end -->

<script type="text/javascript">
(Apex = {
    chart: { parentHeightOffset: 0, toolbar: { show: !1 } },
    grid: { padding: { left: 0, right: 0 } },
    colors: ["#5369f8", "#43d39e", "#f77e53", "#1ce1ac", "#25c2e3", "#ffbe0b"],
    tooltip: { theme: "dark", x: { show: !1 } },
    dataLabels: { enabled: !1 },
    xaxis: { axisBorder: { color: "#d6ddea" }, axisTicks: { color: "#d6ddea" } },
    yaxis: { labels: { offsetX: -10 } },
}),
    (function (e) {
        "use strict";
        function t() {}
        (t.prototype.initCharts = function () {
            e = {
                chart: { height: 380, type: "line", shadow: { enabled: !1, color: "#bbb", top: 3, left: 2, blur: 3, opacity: 1 } },
                stroke: { width: 5, curve: "smooth" },
                series: [{ name: "Subscriptions", data: [<?php 
                                            foreach ($data->analytics->last_x_days_revenues as $value) {
                                                echo $value->total_subscriptions.',';
                                            }

                                            ?>] }],
                xaxis: {
                    // type: "datetime",
                    categories: [
                        <?php foreach ($data->analytics->last_x_days_revenues as $key => $value)       {
                        echo '"'.$value->day.'"'.',';
                            } 
                            ?>
                    ],
                },
                title: { text: "", align: "left", style: { fontSize: "14px", color: "#666" } },
                fill: { type: "gradient", gradient: { shade: "dark", gradientToColors: ["#43d39e"], shadeIntensity: 1, type: "vertical", opacityFrom: 1, opacityTo: 1, stops: [0, 100, 100, 100] } },
                markers: { size: 4, opacity: 0.9, colors: ["#50a5f1"], strokeColor: "#fff", strokeWidth: 2, style: "inverted", hover: { size: 7 } },
                yaxis: { min: -10, max: 40, title: { text: "Subscribers" } },
                grid: { row: { colors: ["transparent", "transparent"], opacity: 0.2 }, borderColor: "#185a9d" },
                responsive: [{ breakpoint: 600, options: { chart: { toolbar: { show: !1 } }, legend: { show: !1 } } }],
            };
            new ApexCharts(document.querySelector("#overall_subscription_chart"), e).render();

            e = {
                chart: { height: 380, type: "bar", toolbar: { show: !1 } },
                plotOptions: { bar: { dataLabels: { position: "top" } } },
                dataLabels: {
                    enabled: !0,
                    formatter: function (e) {
                        return e + "%";
                    },
                    offsetY: -30,
                    style: { fontSize: "12px", colors: ["#304758"] },
                },
                series: [{ name: "Users", data: [<?php 
                                            foreach ($data->analytics->last_x_days_revenues as $value) {
                                                echo $value->total_users.',';
                                            }

                                            ?>] }],
                xaxis: {
                    categories: [<?php foreach ($data->analytics->last_x_days_revenues as $key => $value)       {
                        echo '"'.$value->date.'"'.',';
                    } 
                    ?>],
                    position: "top",
                    labels: { offsetY: -18 },
                    axisBorder: { show: !1 },
                    axisTicks: { show: !1 },
                    crosshairs: { fill: { type: "gradient", gradient: { colorFrom: "#D8E3F0", colorTo: "#BED1E6", stops: [0, 100], opacityFrom: 0.4, opacityTo: 0.5 } } },
                    tooltip: { enabled: !0, offsetY: -35 },
                },
                fill: { gradient: { enabled: !1, shade: "light", type: "horizontal", shadeIntensity: 0.25, gradientToColors: void 0, inverseColors: !0, opacityFrom: 1, opacityTo: 1, stops: [50, 0, 100, 100] } },
                yaxis: {
                    axisBorder: { show: !1 },
                    axisTicks: { show: !1 },
                    labels: {
                        show: !1,
                        formatter: function (e) {
                            return e + "%";
                        },
                    },
                },
                title: { text: "Latest users visited to application", floating: !0, offsetY: 350, align: "center", style: { color: "#444" } },
                grid: { row: { colors: ["transparent", "transparent"], opacity: 0.2 }, borderColor: "#f1f3fa" },
            };
            new ApexCharts(document.querySelector("#recently_visited_users_chart"), e).render();

        }),
            (t.prototype.init = function () {
                this.initCharts();
            }),
            (e.ApexChartPage = new t()),
            (e.ApexChartPage.Constructor = t);
    })(window.jQuery),
    (function () {
        "use strict";
        window.jQuery.ApexChartPage.init();
    })();
</script>

@endsection

