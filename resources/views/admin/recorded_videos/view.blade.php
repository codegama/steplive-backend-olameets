@extends('layouts.admin') 

@section('content-header', tr('recorded_videos'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.meetings.recorded_videos')}}">{{tr('recorded_videos')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('video_lists') }}</span>
    </li> 
           
@endsection 

@section('styles')

<link href="https://unpkg.com/video.js/dist/video-js.css" rel="stylesheet">

<link href="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.7.3/video-js.css" rel="stylesheet">

@endsection

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-body">

                 <h4 class="text-uppercase">{{tr('video_lists')}}
                </h4>

                <div class="card">
                	<b>{{$meeting_record_video_details->meetingDetails->title ?? "-"}}</b>
                	<br>
                	{{tr('total_views')}} - <span>{{$meeting_record_video_details->no_of_views}}</span>
                </div>

				<video
				    id="my-player"
				    class="video-js"
				    controls
				    preload="auto"
				    poster="{{asset('meeting-placeholder.jpg')}}"
				    data-setup='{}' width="900" height="400">
					  	<source src="{{$meeting_record_video_details->video}}" type="video/mp4"></source>
				</video>
                
            </div>
            
        </div>
       
    </div>
   
</div>
    
@endsection

@section('scripts')

<script src="https://unpkg.com/video.js/dist/video.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/video.js/6.7.3/video.js"></script>

@endsection