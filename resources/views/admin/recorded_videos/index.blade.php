@extends('layouts.admin') 

@section('content-header', tr('recorded_videos'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.meetings.recorded_videos')}}">{{tr('recorded_videos')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('video_lists') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-body">

                <div class="border-bottom pb-3">

                    <h5 class="text-uppercase">{{tr('video_lists')}}
                  
                    </h5>

                </div>

                <div class="pt-2 pb-2">
                
                    <table  id="basic-datatable" class="table dt-responsive nowrap">

                        <thead>

                            <tr>
                                <th>{{tr('s_no')}}</th>
                                <th>{{tr('unique_id')}}</th>
                                <th>{{tr('meeting')}}</th>
                                <th>{{tr('meeting_type')}}</th>
                                <th>{{tr('duration')}}</th>
                                <th>{{tr('status')}}</th>
                                <th>{{tr('action')}}</th>
                            </tr>

                        </thead>

                        <tbody>

                            @foreach($meeting_recorded_videos as $i => $meeting_recorded_video_details)
                                 
                                <tr>
                                    <td>{{$i+$meeting_recorded_videos->firstItem()}}</td>

                                    <td>
                                        <a href="{{route('admin.meetings.view',['meeting_id' => $meeting_recorded_video_details->id])}}"> {{ $meeting_recorded_video_details->unique_id }}
                                        </a>
                                    </td>
                                   
                                    <td> 
                                        {{ $meeting_recorded_video_details->meetingDetails->title ?? "-" }} 
                                    </td>

                                    <td>

                                        @switch($meeting_recorded_video_details->meeting_type)

                                            @case(MEETING_TYPE_UPCOMING)
                                                <span class="badge badge-success">{{ tr('upcoming') }} </span>
                                                @break

                                            @case(MEETING_TYPE_LIVE)
                                                <span class="badge badge-primary">{{ tr('onlive') }} </span>
                                                @break

                                            @case(MEETING_TYPE_ENDED)
                                                <span class="badge badge-secondary">{{ tr('ended') }} </span>
                                                @break

                                            @default
                                                 <span class="badge badge-danger">{{ tr('cancelled') }} </span>
                                                @break

                                        @endswitch

                                    </td>
                                   
                                    <td>
                                        <span class="badge badge-secondary">{{ $meeting_recorded_video_details->duration }}</span>
                                    </td>

                                    <td>

                                        @if($meeting_recorded_video_details->status == APPROVED)

                                            <span class="badge badge-success">{{ tr('approved') }} </span>

                                        @else

                                            <span class="badge badge-danger">{{ tr('declined') }} </span>

                                        @endif

                                    </td>

                                    <td>
                                        <a class="btn btn-outline-primary" href="{{ route('admin.meetings.recorded_videos_view', ['video_id' => $meeting_recorded_video_details->id]) }}">
                                            {{tr('view')}}
                                        </a>

                                    </td>

                                </tr>

                            @endforeach
                            
                        </tbody>
                       
                    </table>

                    <div class="float-right">{{ $meeting_recorded_videos->appends(request()->input())->links() }}</div>

                </div>
                
            </div>
            
        </div>
       
    </div>
   
</div>


@endsection