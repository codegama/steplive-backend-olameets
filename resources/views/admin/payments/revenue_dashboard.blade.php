@extends('layouts.admin') 

@section('content-header', tr('payments'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.revenue.dashboard')}}">{{tr('payments')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('revenue_dashboard') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row col-lg-12">

    <div class="col-md-6 col-xl-3">

        <div class="card">

            <div class="card-body p-0">

                <div class="media p-3">

                    <div class="media-body">

                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{tr('total_subscribers')}}</span>
                        <h2 class="mb-0">{{$data->total_subscribers}}</h2>

                    </div>

                    <div class="align-self-center">
                        <span class="icon-lg icon-dual-primary" data-feather="users"></span>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="col-md-6 col-xl-3">

        <div class="card">

            <div class="card-body p-0">

                <div class="media p-3">

                    <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{tr('today_subscribers')}}</span>
                        <h2 class="mb-0">{{$data->today_subscribers}}</h2>
                    </div>

                    <div class="align-self-center">
                      <span class="icon-lg icon-dual-success" data-feather="user"></span>
                    </div>

                </div>

            </div>

        </div>

   </div>
   
   <div class="col-md-6 col-xl-3">

        <div class="card">

            <div class="card-body p-0">

                <div class="media p-3">

                    <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{tr('total_earnings')}}</span>
                        <h2 class="mb-0">{{formatted_amount($data->total_earnings)}}</h2>
                    </div>
                    <div class="align-self-center">
                      <span class="icon-lg icon-dual-info" data-feather="bar-chart"></span>
                    </div>

                </div>

            </div>
        
        </div>
    
    </div>

    <div class="col-md-6 col-xl-3">

        <div class="card">

            <div class="card-body p-0">

                <div class="media p-3">

                    <div class="media-body">
                        <span class="text-muted text-uppercase font-size-12 font-weight-bold">{{tr('today_earnings')}}</span>
                        <h2 class="mb-0">{{formatted_amount($data->today_earnings)}}</h2>
                    </div>

                    <div class="align-self-center">
                      <span class="icon-lg icon-dual-warning" data-feather="inbox"></span>
                    </div>

                </div>

            </div>
        
        </div>
    
    </div>

</div>

<div class="row">

    <div class="col-xl-12">

        <div class="card">

            <div class="card-body">

                <h4 class="header-title mt-0 mb-3">{{tr('last_week_revenues')}}</h4>

                <div id="last_week_revenue" class="apex-charts" dir="ltr"></div>

            </div>

        </div>

    </div> 

</div>


@endsection

@section('scripts')

<script src="https://apexcharts.com/samples/assets/irregular-data-series.js"></script>

<script src="https://apexcharts.com/samples/assets/series1000.js"></script>

<script src="https://apexcharts.com/samples/assets/ohlc.js"></script>

<!-- third party:js -->
<script src="{{asset('admin-assets/assets/libs/apexcharts/apexcharts.min.js')}}"></script>
<!-- third party end -->

<script type="text/javascript">
(Apex = {
    chart: { parentHeightOffset: 0, toolbar: { show: !1 } },
    grid: { padding: { left: 0, right: 0 } },
    colors: ["#5369f8", "#43d39e", "#f77e53", "#1ce1ac", "#25c2e3", "#ffbe0b"],
    tooltip: { theme: "dark", x: { show: !1 } },
    dataLabels: { enabled: !1 },
    xaxis: { axisBorder: { color: "#d6ddea" }, axisTicks: { color: "#d6ddea" } },
    yaxis: { labels: { offsetX: -10 } },
}),
    (function (e) {
        "use strict";
        function t() {}
        (t.prototype.initCharts = function () {
            e = {
                chart: { height: 380, type: "line", shadow: { enabled: !1, color: "#bbb", top: 3, left: 2, blur: 3, opacity: 1 } },

                series: [{ name: "Subscriptions", data: [<?php 
                                            foreach ($data->analytics->last_x_days_revenues as $value) {
                                                echo $value->total_subscriptions.',';
                                            }

                                            ?>] }],
                xaxis: {
                    // type: "datetime",
                    categories: [
                        <?php foreach ($data->analytics->last_x_days_revenues as $key => $value)       {
                        echo '"'.$value->day.'"'.',';
                            } 
                            ?>
                    ],
                },
               
            };
           

            e = {
                chart: { height: 380, type: "bar", toolbar: { show: !1 } },
                plotOptions: { bar: { dataLabels: { position: "top" } } },
                dataLabels: {
                    enabled: !0,
                    formatter: function (e) {
                        return e + "%";
                    },
                    offsetY: -30,
                    style: { fontSize: "12px", colors: ["#304758"] },
                },
                series: [{ name: "Amount", data: [<?php 
                                            foreach ($data->analytics->last_x_days_revenues as $value) {
                                                echo $value->total_earnings.',';
                                            }

                                            ?>] }],
                xaxis: {
                    categories: [<?php foreach ($data->analytics->last_x_days_revenues as $key => $value)       {
                        echo '"'.$value->date.'"'.',';
                    } 
                    ?>],
                    position: "top",
                    labels: { offsetY: -18 },
                    axisBorder: { show: !1 },
                    axisTicks: { show: !1 },
                    crosshairs: { fill: { type: "gradient", gradient: { colorFrom: "#D8E3F0", colorTo: "#BED1E6", stops: [0, 100], opacityFrom: 0.4, opacityTo: 0.5 } } },
                    tooltip: { enabled: !0, offsetY: -35 },
                },
                fill: { gradient: { enabled: !1, shade: "light", type: "horizontal", shadeIntensity: 0.25, gradientToColors: void 0, inverseColors: !0, opacityFrom: 1, opacityTo: 1, stops: [50, 0, 100, 100] } },
                yaxis: {
                    axisBorder: { show: !1 },
                    axisTicks: { show: !1 },
                    labels: {
                        show: !1,
                        formatter: function (e) {
                            return "{{Setting::get('currency')}}" + e;
                        },
                    },
                },
                title: { text: "Last week revenue", floating: !0, offsetY: 350, align: "center", style: { color: "#444" } },
                grid: { row: { colors: ["transparent", "transparent"], opacity: 0.2 }, borderColor: "#f1f3fa" },
            };
            new ApexCharts(document.querySelector("#last_week_revenue"), e).render();

        }),
            (t.prototype.init = function () {
                this.initCharts();
            }),
            (e.ApexChartPage = new t()),
            (e.ApexChartPage.Constructor = t);
    })(window.jQuery),
    (function () {
        "use strict";
        window.jQuery.ApexChartPage.init();
    })();
</script>

@endsection