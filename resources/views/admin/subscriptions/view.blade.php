@extends('layouts.admin') 

@section('content-header', tr('subscriptions'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{route('admin.subscriptions.index')}}">{{tr('subscriptions')}}</a>
    </li>
    <li class="breadcrumb-item active" aria-current="page">
        <span>{{tr('view_subscriptions')}}</span>
    </li>
           
@endsection  

@section('content')

<div class="col-12">

	<div class="card">
	    
	    <div class="card-body">

      		 <div class="border-bottom pb-3">

                <h5 class="text-uppercase">{{tr('view_subscriptions')}}  - {{$subscription_details->title}}
              
                </h5>

            </div>

      		<div class="row py-1">

               	<div class="col-xl-3 col-sm-6">

                  	<div class="media p-3">

                     	<i data-feather="grid" class="align-self-center icon-dual icon-lg mr-4"></i>

	                    <div class="media-body">
	                        <h4 class="mt-0 mb-0">{{$subscription_details->title}}</h4>
	                        <span class="text-muted font-size-13">{{tr('title')}}</span>
	                    </div>

                  	</div>

               	</div>

               	<div class="col-xl-3 col-sm-6">

	                  	<div class="media p-3">

                     	<i data-feather="clock" class="align-self-center icon-dual icon-lg mr-4"></i>

	                    <div class="media-body">
	                        <h4 class="mt-0 mb-0">{{$subscription_details->plan_formatted}}</h4>
	                        <span class="text-muted">{{tr('plan')}}</span>
	                    </div>

                  	</div>

               	</div>

               	<div class="col-xl-3 col-sm-6">

                 	<div class="media p-3">

                     	<i data-feather="check-square"
                        class="align-self-center icon-dual icon-lg mr-4"></i>

                    	<div class="media-body">
                        	<h4 class="mt-0 mb-0">{{$subscription_details->subscriptionPayments()->count()}}</h4>
                        	<span class="text-muted">{{tr('total_subscribers')}}</span>
                     	</div>

                  	</div>

               	</div>

               	<div class="col-xl-3 col-sm-6">

                  	<div class="media p-3">

                    <i data-feather="credit-card"
                        class="align-self-center icon-dual icon-lg mr-4"></i>
                     	<div class="media-body">
	                        <h4 class="mt-0 mb-0">{{$subscription_details->amount_formatted}}</h4>
	                        <span class="text-muted">{{tr('total_amount_spent')}}</span>
                     	</div>

                  	</div>

               	</div>

	        </div>

	      	<div class="row">

	        	<div class="col-6">

	        		<div class="card">

		        		<div class="card-body">

		        			<ul class="nav flex-column">
		        				
		        				<li class="nav-item">
		        					{{tr('description')}}
		        				
	        						<div  class=" text-word-wrap">{{$subscription_details->description}}
	        						</div>
	        				
		        				</li>
		        		
		        			</ul>

		        			<hr>

		        			<h5 class="text-uppercase">{{tr('action')}}</h5>

	        				@if(Setting::get('admin_delete_control') == YES )

	        					<div class="row">

	        						<div class="col-md-4">

						      			<a href="{{ route('admin.subscriptions.edit', ['subscription_id' => $subscription_details->id] ) }}" class="btn btn-block btn-warning" title="{{tr('edit')}}"><b>{{tr('edit')}}</b></a>

						      		</div>

						      		<div class="col-md-4">

						      			<a onclick="return confirm(&quot;{{ tr('subscription_delete_confirmation', $subscription_details->title ) }}&quot;);" href="javascript:;" class="btn btn-block btn-danger" title="{{tr('delete')}}"><b>{{tr('delete')}}</b>
						      			</a>

						      		</div>

					      		</div>


					   		@else

					   		<div class="row pb-3">

	        					<div class="col-md-4">

					   				<a href="{{ route('admin.subscriptions.edit' , ['subscription_id' => $subscription_details->id] ) }}" class="btn btn-block btn-warning" title="{{tr('edit')}}"><b>{{tr('edit')}}</b></a>

					   			</div>	

					   			<div class="col-md-4">
					      		                			
					      	 		<a onclick="return confirm(&quot;{{ tr('subscription_delete_confirmation', $subscription_details->title ) }}&quot;);" href="{{ route('admin.subscriptions.delete', ['subscription_id' => $subscription_details->id] ) }}" class="btn btn-block btn-danger" title="{{tr('delete')}}"><b>{{tr('delete')}}</b>
					      			</a>

					      		</div>

					      	</div>
					      	@endif

					      	<div class="row">

					      		<div class="col-md-4">

							      	@if($subscription_details->status == APPROVED)
							      	
						                <a class="btn btn-block btn-danger" title="{{tr('decline')}}" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}" onclick="return confirm(&quot;{{$subscription_details->title}} - {{tr('subscription_decline_confirmation')}}&quot;);" >
						                    <b>{{tr('decline')}}</b>
						                </a>

						            @else
						                
						                <a class="btn btn-block btn-success" title="{{tr('approve')}}" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}">
						                    <b>{{tr('approve')}}</b> 
						                </a>
						                   
						            @endif

					            </div>

				            </div>
		        			
		                </div>

		            </div>

	        	</div>

	        	<div class="col-6">

	        		<div class="card">
	        			
		        		<div class="card-body">

		        			<ul class="nav flex-column">

		        				<li class="nav-item">
		        					{{tr('is_popular')}}
		        					@if($subscription_details->is_popular == YES)
		        						<span class="float-right badge badge-success">{{tr('yes')}}</span>
		        					@else
		        						<span class="float-right badge badge-danger">
		        							{{tr('no')}}
		        						</span>
		        					@endif
		        				</li>
		        				<hr>
		        			
		        				<li class="nav-item">
		        					{{tr('is_free')}}
		        					@if($subscription_details->is_free == YES)
		        						<span class="float-right badge badge-success">{{tr('yes')}}</span>
		        					@else
		        						<span class="float-right badge badge-danger">
		        							{{tr('no')}}
		        						</span>
		        					@endif
		        				</li>
		        				<hr>

		        				<li class="nav-item">
		        					{{tr('status')}}
		        					@if($subscription_details->status == YES)
		        						<span class="float-right badge badge-success">
		        						{{tr('approved')}}
		        						</span>
		        					@else
		        						<span class="float-right badge badge-danger">
		        							{{tr('declined')}}
		        						</span>
		        					@endif
		        				</li>
		        				<hr>

		        				<li class="nav-item">
		        					{{tr('created_at')}}
		        					<span class="float-right"> {{common_date($subscription_details->created_at,Auth::guard('admin')->user()->timezone)}}</span>
		        				</li>
		        				<hr>

		        				<li class="nav-item">
		        					{{tr('updated_at')}}
		        					<span class="float-right"> {{common_date($subscription_details->updated_at,Auth::guard('admin')->user()->timezone)}}</span>
		        				</li>
		        			</ul>
		                	
		                </div>

	                </div>

	        	</div>

	      	</div>

	    </div>

	</div>

</div>

@endsection