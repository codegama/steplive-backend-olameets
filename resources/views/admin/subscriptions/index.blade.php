@extends('layouts.admin') 

@section('content-header', tr('subscriptions'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.subscriptions.index')}}">{{tr('subscriptions')}}</a></li>

    <li class="breadcrumb-item active" aria-current="page">
        <span>{{ tr('view_subscriptions') }}</span>
    </li> 
           
@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                <div class="border-bottom pb-3">

                    <h5 class="text-uppercase">{{tr('view_subscriptions')}}

                        <a class="btn btn-outline-primary float-right" href="{{route('admin.subscriptions.create')}}">
                            <i class="fa fa-plus"></i> {{tr('add_subscription')}}
                        </a>
                  
                    </h5>

                </div>

                <div class="pt-2 pb-2">

                    <table  id="basic-datatable" class="table dt-responsive nowrap">

                        <thead>
                            <tr>
                                <th>{{tr('s_no')}}</th>
                                <th>{{tr('title')}}</th>
                                <th>{{tr('no_of_users')}}</th>
                                <th>{{tr('no_of_minutes')}}</th>
                                <th>{{tr('subscribers')}}</th>
                                <th>{{tr('status')}}</th>
                                <th>{{tr('amount')}}</th>
                                <th>{{tr('action')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($subscriptions as $i => $subscription_details)
                                  
                                <tr>
                                    <td>{{$i+$subscriptions->firstItem()}}</td>

                                    <td>
                                        <a href="{{route('admin.subscriptions.view' , ['subscription_id' => $subscription_details->id])}}"> {{ $subscription_details->title }}
                                        </a>
                                    </td>

                                    <td>{{$subscription_details->no_of_users_formatted}}</td>
                                    
                                    <td>{{$subscription_details->no_of_minutes_formatted}}</td>

                                    <td><a href="{{route('admin.subscription_payments.index',['subscription_id' => $subscription_details->id])}}">{{$subscription_details->subscriptionPayments()->count()}}</a></td>

                                    <td>

                                        @if($subscription_details->status == APPROVED)

                                            <span class="badge badge-success">{{ tr('approved') }} </span>

                                        @else

                                            <span class="badge badge-danger">{{ tr('declined') }} </span>

                                        @endif

                                    </td>
                                        
                                    <td>  
                                        {{formatted_amount ($subscription_details->amount)}}           
                                    </td>

                                    <td>     

                                        <div class="template-demo">

                                            <div class="dropdown">

                                                <button class="btn btn-outline-primary  dropdown-toggle" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    {{tr('action')}}
                                                </button>

                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                                                  
                                                    <a class="dropdown-item" href="{{ route('admin.subscriptions.view', ['subscription_id' => $subscription_details->id]) }}">
                                                        {{tr('view')}}
                                                    </a>
                                                    
                                                    @if(Setting::get('is_demo_control_enabled') == NO)
                                                        <a class="dropdown-item" href="{{ route('admin.subscriptions.edit', ['subscription_id' => $subscription_details->id]) }}">
                                                            {{tr('edit')}}
                                                        </a>
                                                        
                                                        <a class="dropdown-item" href="{{route('admin.subscriptions.delete', ['subscription_id' => $subscription_details->id])}}" 
                                                        onclick="return confirm(&quot;{{tr('subscription_delete_confirmation' , $subscription_details->title)}}&quot;);">
                                                            {{tr('delete')}}
                                                        </a>
                                                    @else

                                                        <a class="dropdown-item text-muted" href="javascript:;" >{{tr('edit')}}</a>
                                                      
                                                        <a class="dropdown-item text-muted" href="javascript:;" onclick="return confirm(&quot;{{tr('subscription_delete_confirmation' , $subscription_details->title)}}&quot;);">{{tr('delete')}}</a>                           
                                                    @endif

                                                    <div class="dropdown-divider"></div>


                                                    @if($subscription_details->status == APPROVED)

                                                        <a class="dropdown-item" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}" onclick="return confirm(&quot;{{$subscription_details->title}} - {{tr('subscription_decline_confirmation')}}&quot;);" >
                                                            {{ tr('decline') }} 
                                                        </a>

                                                    @else
                                                        
                                                        <a class="dropdown-item" href="{{ route('admin.subscriptions.status', ['subscription_id' => $subscription_details->id]) }}">
                                                            {{ tr('approve') }} 
                                                        </a>
                                                           
                                                    @endif


                                                </div>

                                            </div>

                                        </div>

                                    </td>

                                </tr>

                            @endforeach
                            
                        </tbody>
                       
                    </table>

                </div>
            </div>
            
        </div>
        
    </div>
   
</div>


@endsection